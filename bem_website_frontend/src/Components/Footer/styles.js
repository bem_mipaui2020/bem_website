import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  height: auto;
  margin-top: 100px;
  background-color: #262626;
  // padding-top: 30px;
  // padding-left: 100px;
  color: white;

  .grid {
    margin-top: 30px;
    margin-left: 100px;
  }
`;
