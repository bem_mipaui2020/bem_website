import React from "react";
import { Button, Grid, TextField } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import InstagramIcon from "@material-ui/icons/Instagram";
import RoomIcon from "@material-ui/icons/Room";
import EmailIcon from "@material-ui/icons/Email";
import CallIcon from "@material-ui/icons/Call";
import { makeStyles } from "@material-ui/core/styles";
import { Wrapper } from "./styles";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      color: "white",
    },
    "& .MuiInput-underline:before": {
      borderBottomColor: "white",
      "&:hover": {
        borderBottomColor: "yellow",
      },
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#BB4628",
    },
    "& label.Mui-focused": {
      color: "white",
    },
  },
  name: {
    "& > *": {
      color: "white",
    },
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Footer = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <Wrapper>
      <Grid container>
        <Grid item md={2} className="grid">
          <h2>Ikuti Kami</h2>
          <Grid container direction="row">
            <Grid item md={3}>
              <FacebookIcon fontSize="large" />
            </Grid>
            <Grid item md={3}>
              <TwitterIcon fontSize="large" />
            </Grid>
            <Grid item md={3}>
              <InstagramIcon fontSize="large" />
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={3} className="grid">
          <h2>Kontak</h2>
          <Grid container direction="column">
            <Grid container direction="row">
              <Grid item md={2}>
                <RoomIcon style={{ marginTop: "30px" }} />
              </Grid>
              <Grid item md={8}>
                <h4>
                  FMIPA UI, Pondok Cina, Kecamatan Beji, Kota Depok, Jawa Barat
                  16424
                </h4>
              </Grid>
            </Grid>
            <Grid container direction="row">
              <Grid item md={2}>
                <EmailIcon style={{ marginTop: "20px" }} />
              </Grid>
              <Grid item md={8}>
                <h4>bem@sci.ui.ac.id</h4>
              </Grid>
            </Grid>
            <Grid container direction="row">
              <Grid item md={2}>
                <CallIcon style={{ marginTop: "20px" }} />
              </Grid>
              <Grid item md={8}>
                <h4>087786394579 (Dewi)</h4>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={4} className="grid">
          <h2>Let's Get In Touch</h2>
          <Grid
            container
            direction="column"
            justify="space-between"
            style={{ paddingBottom: "30px" }}
          >
            <TextField
              id="standard-basic"
              label="First Name"
              className={classes.root}
            />
            <TextField
              id="standard-basic"
              label="Subject"
              className={classes.root}
            />
            <TextField
              id="standard-basic"
              label="Email"
              className={classes.root}
            />
            <TextField
              id="standard-basic"
              label="Message"
              className={classes.root}
              style={{ marginTop: "20px" }}
            />
            <Grid item md={3}>
              <Button
                onClick={handleClick}
                variant="contained"
                color="primary"
                style={{ marginTop: "20px" }}
              >
                Submit
              </Button>
            </Grid>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
              <Alert onClose={handleClose} severity="success">
                This is a success message!
              </Alert>
            </Snackbar>
          </Grid>
        </Grid>
      </Grid>
    </Wrapper>
  );
};

export default Footer;
