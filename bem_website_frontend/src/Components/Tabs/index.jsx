import React from 'react';
import PropTypes from 'prop-types';
import {Button, Paper, Grid} from "@material-ui/core"
import {Wrapper, Flex} from "./styles"
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import SwipeableViews from 'react-swipeable-views';

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-auto-tabpanel-${index}`}
        aria-labelledby={`scrollable-auto-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

  function a11yProps(index) {
    return {
      id: `scrollable-auto-tab-${index}`,
      'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
  }
  
    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
            width: '100%',
            backgroundColor: theme.palette.background.paper,
        },
        text:{
            color: "black",
            textTransform: "Capitalize"
        }
    }));

// const useStyles = makeStyles((theme) => ({
//     root: {
//     // flexGrow: 1,
//     // width: 300,
//     // backgroundColor: theme.palette.background.paper,
//     },
//     app:{
//         background: "white"
//     },
//     text:{
//         color: "black",
//         textTransform: "Capitalize"
//     }
// }));

export default function SimpleTabs() {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleChangeIndex = (index) => {
    setValue(index);
  };


    const kewirausahaan = [
        {title: "Jaket Bem", value: "Program pengadaan jaket BEM sebagai identitas bagi seluruh fungsionaris BEM FMIPA UI 2020." },
        {title: "Merchandise FMIPA UI", value: "Program pengadaan merchandise FMIPA, berupa kaos, jaket, totebag, dll, sebagai kebanggaan mahasiswa FMIPA." },
        {title: "MIPA Preloved", value: "Program menjual pakaian preloved layak pakai yang dihimpun dari fungsionaris BEM FMIPA UI 2020 dan mahasiswa FMIPA." },
        {title: "Bazar Biru Hitam", value: "Program berupa bazar makanan dan minuman oleh mahasiswa FMIPA dan sarana bagi MIPA-Preneur dalam memperkenalkan produknya." },
        {title: "TOSSAKA", value: "Program berupa try out, bedah fakultas, seminar motivasi, dan food bazaar bagi siswa/i sederajat, maupun alumni." },
        {title: "Wirus Market", value: "Program penyediaan dan penjualan barang sesuai dengan tujuan yang diinginkan oleh biro/departemen BEM FMIPA UI 2020 yang bersangkutan." },
        {title: "MIPA Preneur", value: "Program yang menjadi wadah bagi mahasiswa FMIPA dalam memperkenalkan dan menjenamakan usahanya. " },
        {title: "YED (Young Entrepreneur Days)", value: "Program berupa rangkaian seminar kewirausahaan." },
    ]

    const kesekretariatan = [
        {title: "Bata BEM (Basis Data BEM)", value: "Pengarsipan data diri seluruh fungsionaris BEM FMIPA UI 2020" },
        {title: "Publikasi SOP", value: "Pengumpulan dan publikasi seluruh SOP bidang" },
        {title: "Surat-Menyurat", value: "Pembuatan dan pengarsipan surat masuk dan surat keluar BEM FMIPA UI" },
        {title: "Kalender Acara Kita", value: "Publikasi pelaksanaan kegiatan yang dibawahi oleh BEM FMIPA UI 2020 melalui mading yang ditempel di Ruang BEM dan grup LINE BEM FMIPA UI 2020" },
        {title: "Bangun Rumah", value: "Kegiatan membersihkan dan merapikan inventaris di Ruang BEM bersama seluruh fungsionaris BEM FMIPA UI 2020" },
        {title: "Kemah BEM (Kita Bersih-Bersih Rumah BEM)", value: "Piket bergilir oleh tiap bidang di BEM FMIPA UI 2020 untuk membersihkan Ruang BEM" },
        {title: "Rental BEM", value: "Piket bergilir oleh tiap bidang di BEM FMIPA UI 2020 untuk membersihkan Ruang BEM" },
        {title: "Semua Tentang BEM", value: "Media berisi visi, misi, nilai, struktur kepengurusan, dan SOP BEM FMIPA UI 2020" },
        {title: "Arsip Terpadu", value: "Pengarsipan dokumen-dokumen kegiatan yang berkaitan dengan BEM FMIPA UI 2020, seperti notula rapat bidang, proposal, dan LPJ" },
        {title: "Apresiasi BEM", value: "Bentuk apresiasi dan penghargaan untuk seluruh fungsionaris BEM FMIPA UI 2020 berupa sertifikat dan bentuk lainnya" },
        
    ]

    const penelitian = [
        {title: "Need's Assessment", value: "Pelayanan untuk membantu pembuatan kuisioner kepada biro/departemen yang membutuhkan terhadap penilaian tertentu"},
        {title: "P2K (Penelitian Program Kerja)", value: "Evaluasi terhadap program kerja dengan UKK yang diselenggarakan oleh BEM FMIPA UI 2020"},
        {title: "MIPA Leaderboard", value: "Platform pendataan seluruh kegiatan kemahasiswaan baik di bidang prestasi lomba, non lomba, pengabdian masyarakat, dan kegiatan lainnya yang memberikan manfaat bagi FMIPA, UI, dan Indonesia"},
        {title: "MIPA Open Data", value: "Wadah terpusat yang berisi data keseluruhan BEM FMIPA UI dan distribusi mahasiswa FMIPA UI"},
        {title: "Rapor BEM", value: "Penilaian bersifat kuantitatif yang diisi oleh seluruh fungsionaris BEM FMIPA UI 2020 "},

    ]
    const sda = [
        {title: "KITKAT (Kumpul Bareng Internal - Konsolidasi Awal Tahun)", value: "Pengenalan organisasi serta seluruh fungsionaris BEM FMIPA UI 2020 dengan melakukan berbagai kegiatan, yaitu penyambutan staf, musyawarah kerja, dan team building "},
        {title: "Kondangan, yuk! (Konsolidasi Tengah Tahun, yuk!)", value: "Berbagai kegiatan yang dilakukan di luar kampus untuk menguatkan kembali internal BEM FMIPA UI 2020"},
        {title: "Farewell BEM FMIPA UI 2020", value: "Acara perpisahan bagi seluruh fungsionaris BEM FMIPA UI 2020 "},
        {title: "Birthday Card", value: "Ucapan ulang tahun dalam bentuk foto dan fun fact dari fungsionaris yang berulang tahun yang kemudian disebarkan ke grup LINE BEM FMIPA UI 2020"},
        {title: "FUNDAY", value: "Kumpul bersama fungsionaris BEM FMIPA UI 2020 melakukan kegiatan seperti futsal, bulu tangkis, dan car free day. Kegiatan ini dapat berkolaborasi dengan Biro/Departemen BEM FMIPA UI 2020 lainnya."},
        {title: "Buku Penghubung", value: "Borang yang berisi perkembangan dari masing-masing bidang di BEM FMIPA UI 2020 beserta seluruh fungsionaris di dalamnya, yang diisi oleh sahabat bidang. Data yang diberikan saat penilaian adalah data kualitatif"},
        {title: "Sahabat Bidang", value: "Penempatan satu fungsionaris PSDM pada setiap bidang di BEM FMIPA UI 2020"},
        {title: "ASK FM (Ask Fungsionaris MIPA)", value: "Ajang tanya jawab santai di grup LINE BEM FMIPA UI 2020 "},
        {title: "WIB (Waktu Internal Bercanda)", value: "Pemberian konten humor di grup LINE BEM FMIPA UI 2020 dalam rentang waktu yang ditentukan"},
        {title: "Homecoming Party", value: "Berkumpul bersama seluruh fungsionaris BEM FMIPA UI 2020 dengan suasana seperti penyambutan setelah sekian lama melakukan Pembelajaran Jarak Jauh (PJJ)"},

    ]
    const humas = [
        {title: "Media Sosial", value: "Media informasi dalam jaringan yang digunakan untuk menyebarkan informasi BEM FMIPA UI secara non fisik"},
        {title: "Peluncuran Akbar", value: "Kegiatan penyambutan dan perkenalan pengurus baru BEM FMIPA UI 2020 ke seluruh lembaga di FMIPA UI, BEM fakultas lain, dan lembaga tingkat UI."},
        {title: "Dallas TV", value: "Media informasi yang digunakan oleh BEM FMIPA UI 2020 dalam melakukan publikasi berupa gambar ataupun video melalui televisi di kantin FMIPA UI terkait dengan kegiatan internal maupun eksternal BEM FMIPA UI 2020"},
        {title: "Studi Banding", value: "Kegiatan pertemuan dan pertukaran wawasan serta informasi kelembagaan antara BEM FMIPA UI 2020 dengan lembaga kemahasiswaan di luar UI"},
        {title: "Kalender Biru Hitam", value: "Media informasi yang memuat ringkasan agenda internal FMIPA UI yang dapat disebarluaskan menjadi informasi daring maupun luring. "},
        {title: "Majalah Dinding", value: "Media informasi BEM FMIPA UI 2020 dalam berbentuk fisik atau luring"},
        {title: "Humas-Net", value: "Kegiatan pertemuan dengan Humas lembaga se-MIPA dan se-UI untuk melakukan kegiatan kolaborasi program, seperti Welcoming Mahasiswa Baru, Peluncuran Akbar, BKUI, dan MIP(A)ROUD."},
        {title: "MIP(A)ROUD", value: "Kegiatan kolaborasi akhir tahun antara Humas BEM FMIPA UI 2020 dengan Ksatria Biru Hitam 2020 yang diadakan sebagai acara penutupan akbar BEM FMIPA UI dan apresiasi kontingen MT UI"},

    ]
    const humass = [
        {title: "Need's Assessment", value: ""},
        {title: "P2K (Penelitian Program Kerja)", value: ""},
        {title: "P2K (Penelitian Program Kerja)", value: ""},
        {title: "P2K (Penelitian Program Kerja)", value: ""},

    ]
 

  return (
    <Wrapper>
        

    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
            <Tab className={classes.text} label="kewirausahaan" {...a11yProps(0)}  />
            <Tab className={classes.text} label="kesekretariatan" {...a11yProps(1)} />
            <Tab className={classes.text} label="penelitian dan pengembangan" {...a11yProps(2)} />
            <Tab className={classes.text} label="pengembangan sumber daya manusia" {...a11yProps(3)} />
            <Tab className={classes.text} label="Hubungan Masyarakat" {...a11yProps(4)} />
            {/* <Tab className={classes.text} label="hubungan masyarakat" {...a11yProps(5)} /> */}

            
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
            <Flex directionn="row" justify="center" alignItems="center" wrap="wrap" style={{width: '60em'}}>
                {kewirausahaan.map((item) => (
                    <Paper className="paper">
                        <Flex direction="row" justify="center" className="header">
                            <h3>{item.title}</h3>
                        </Flex>
                    <p className="content">{item.value} </p>
                    </Paper>
                ))}
            </Flex>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Flex direction="row" justify="center" alignItems="center" wrap="wrap" style={{width: '60em'}}>
            {kesekretariatan.map((item) => (
                <Paper className="paper">
                    <Flex direction="row" justify="center" className="header">
                        <h3>{item.title}</h3>
                    </Flex>
                    <p className="content">{item.value} </p>
                </Paper>
            ))}
        </Flex>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Flex direction="row" justify="center" alignItems="center" wrap="wrap" style={{width: '60em'}}>
            {penelitian.map((item) => (
                <Paper className="paper">
                    <Flex direction="row" justify="center" className="header">
                        <h3>{item.title}</h3>
                    </Flex>
                    <p className="content">{item.value} </p>
                </Paper>
            ))}
        </Flex>
      </TabPanel>
      <TabPanel value={value} index={3}>
        <Flex direction="row" justify="center" alignItems="center" wrap="wrap" style={{width: '60em'}}>
            {sda.map((item) => (
                <Paper className="paper">
                    <Flex direction="row" justify="center" className="header">
                        <h3>{item.title}</h3>
                    </Flex>
                    <p className="content">{item.value} </p>
                </Paper>
            ))}
        </Flex>
      </TabPanel>
      <TabPanel value={value} index={4}>
        <Flex direction="row" justify="center" alignItems="center" wrap="wrap" style={{width: '60em'}}>
            {humas.map((item) => (
                <Paper className="paper">
                    <Flex direction="row" justify="center" className="header">
                        <h3>{item.title}</h3>
                    </Flex>
                    <p className="content">{item.value} </p>
                </Paper>
            ))}
        </Flex>
      </TabPanel>
    </div>


    

    
    </Wrapper>
  );
}
