import styled from "styled-components";

export const Flex = styled.div`
  display: flex;
  flex-direction: ${({ direction }) => direction};
  justify-content: ${({ justify }) => justify};
  align-items: ${({ alignItems }) => alignItems};
  flex-wrap: ${({ wrap }) => wrap};
`;

export const Wrapper = styled.div`
  * {
    margin: 0;
    padding: 0;
    user-select: none;
    box-sizing: border-box;
    font-family: "Poppins", sans-serif;
  }
  body {
    background: #f2f2f2;
  }
  nav {
    background: white;
  }
  img {
    color: black;
    padding: 1px 80px;
    width: 10em;
    cursor: pointer;
  }
  nav:after {
    content: "";
    clear: both;
    display: table;
  }
  nav .logo {
    float: left;
    color: black;
    font-size: 27px;
    font-weight: 600;
    // line-height: 70px;
    // padding-left: 60px;
  }
  nav ul {
    float: right;
    margin-right: 100px;
    margin-top: 10px;
    list-style: none;
    position: relative;
  }
  nav ul li {
    float: left;
    display: inline-block;
    background: white;
    margin: 0 5px;
  }
  nav ul li a {
    color: black;
    line-height: 70px;
    text-decoration: none;
    font-size: 18px;
    font-weight: 600;
    padding: 8px 15px;
  }
  nav ul li a:hover {
    background: #ede6de;
    border-radius: 5px;
    transition: 0.5s;
  }
  nav ul ul li a:hover {
    box-shadow: none;
  }
  nav ul ul {
    position: absolute;
    top: 20px;
    border-top: 3px solid black;
    opacity: 0;
    visibility: hidden;
    transition: top 0.3s;
  }
  nav ul ul ul {
    border-top: none;
  }
  nav ul li:hover > ul {
    top: 50px;

    opacity: 1;
    visibility: visible;
  }
  nav ul ul li {
    position: relative;
    margin: 0px;
    width: 150px;
    // height: 200px;
    float: none;
    display: list-item;
    // border-bottom: 1px solid rgba(0, 0, 0, 0.3);
  }
  nav ul ul li a {
    line-height: 50px;
    font-size: 14px;
  }
  nav ul ul ul li {
    position: relative;
    top: -60px;
    left: 150px;
  }
  .show,
  .icon,
  input {
    display: none;
  }
  .fa-plus {
    font-size: 15px;
    margin-left: 40px;
  }
  @media all and (max-width: 968px) {
    nav ul {
      margin-right: 0px;
      float: left;
    }
    nav .logo {
      padding-left: 30px;
      width: 100%;
    }
    .show + a,
    ul {
      display: none;
    }
    nav ul li,
    nav ul ul li {
      display: block;
      width: 100%;
    }
    nav ul li a:hover {
      box-shadow: none;
    }
    .show {
      display: block;
      color: white;
      font-size: 18px;
      padding: 0 20px;
      line-height: 70px;
      cursor: pointer;
    }
    .show:hover {
      color: cyan;
    }
    .icon {
      display: block;
      color: white;
      position: absolute;
      top: 0;
      right: 40px;
      line-height: 70px;
      cursor: pointer;
      font-size: 25px;
    }
    nav ul ul {
      top: 70px;
      border-top: 0px;
      float: none;
      position: static;
      display: none;
      opacity: 1;
      visibility: visible;
    }
    nav ul ul a {
      padding-left: 40px;
    }
    nav ul ul ul a {
      padding-left: 80px;
    }
    nav ul ul ul li {
      position: static;
    }
    [id^="btn"]:checked + ul {
      display: block;
    }
    nav ul ul li {
      border-bottom: 0px;
    }
    span.cancel:before {
      content: "\f00d";
    }
  }
  .content {
    z-index: -1;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    text-align: center;
  }
  header {
    font-size: 35px;
    font-weight: 600;
    padding: 10px 0;
  }
  p {
    font-size: 30px;
    font-weight: 500;
  }

  // * {
  //   margin: 0;
  //   padding: 0;
  //   user-select: none;
  //   box-sizing: border-box;
  //   font-family: "Poppins", sans-serif;
  // }

  // nav {
  //   background: white;
  // }
  // img {
  //   color: black;
  //   padding: 1px 80px;
  //   width: 10em;
  //   cursor: pointer;
  // }
  // nav:after {
  //   content: "";
  //   clear: both;
  //   display: table;
  // }
  // nav .logo {
  //   float: left;
  //   color: black;
  //   font-size: 27px;
  //   font-weight: 600;
  //   // line-height: 70px;
  //   // padding-left: 60px;
  // }
  // nav ul {
  //   float: right;
  //   margin-right: 100px;
  //   margin-top: 10px;
  //   list-style: none;
  //   position: relative;
  // }
  // nav ul li {
  //   float: left;
  //   display: inline-block;
  //   // background: #1b1b1b;
  //   // margin: 0 5px;
  // }
  // nav ul li a {
  //   color: black;
  //   line-height: 70px;
  //   text-decoration: none;
  //   font-size: 18px;
  //   font-weight: 600;
  //   padding: 8px 15px;
  // }
  // nav ul li a:hover {
  //   background: #ede6de;
  //   border-radius: 5px;
  //   transition: 0.5s;
  // }
  // nav ul ul li a:hover {
  //   box-shadow: none;
  // }
  // nav ul ul {
  //   position: absolute;
  //   top: 90px;
  //   background: white;
  //   // border-top: 3px solid black;
  //   opacity: 0;
  //   visibility: hidden;
  //   transition: 0.3s;
  // }
  // nav ul ul ul {
  //   // border-top: 3px solid black;
  // }
  // nav ul ul ul li {
  //   position: relative;
  //   background: white;
  //   margin: 0px;
  //   width: 150px;
  //   float: none;
  //   display: list-item;
  //   // border-bottom: 1px solid rgba(0, 0, 0, 0.3);
  // }
  // nav ul li:hover > ul {
  //   top: 50px;
  //   opacity: 1;
  //   visibility: visible;
  // }
  // nav ul ul li {
  //   position: relative;
  //   margin: 0px;
  //   width: 150px;
  //   float: none;
  //   display: list-item;
  //   border-bottom: 1px solid rgba(0, 0, 0, 0.3);
  // }
  // nav ul ul li a {
  //   line-height: 50px;
  //   font-size: 14px;
  // }
  // nav ul ul ul li {
  //   position: relative;
  //   top: -60px;
  //   left: 150px;
  // }
  // .show,
  // .icon,
  // input {
  //   display: none;
  // }
  // .fa-plus {
  //   font-size: 15px;
  //   margin-left: 40px;
  // }
  // @media all and (max-width: 968px) {
  //   nav ul {
  //     margin-right: 0px;
  //     float: left;
  //   }
  //   nav .logo {
  //     padding-left: 30px;
  //     width: 100%;
  //   }
  //   .show + a,
  //   ul {
  //     display: none;
  //   }
  //   nav ul li,
  //   nav ul ul li {
  //     display: block;
  //     width: 100%;
  //   }
  //   nav ul li a:hover {
  //     box-shadow: none;
  //   }
  //   .show {
  //     display: block;
  //     color: black;
  //     font-size: 18px;
  //     padding: 0 20px;
  //     line-height: 70px;
  //     cursor: pointer;
  //   }
  //   .show:hover {
  //     color: cyan;
  //   }
  //   .icon {
  //     display: block;
  //     color: black;
  //     position: absolute;
  //     top: 0;
  //     right: 40px;
  //     line-height: 70px;
  //     cursor: pointer;
  //     font-size: 25px;
  //   }
  //   nav ul ul {
  //     top: 70px;
  //     border-top: 0px;
  //     float: none;
  //     position: static;
  //     display: none;
  //     opacity: 1;
  //     visibility: visible;
  //   }
  //   nav ul ul a {
  //     padding-left: 40px;
  //   }
  //   nav ul ul ul a {
  //     padding-left: 80px;
  //   }
  //   nav ul ul ul li {
  //     position: static;
  //   }
  //   [id^="btn"]:checked + ul {
  //     display: block;
  //   }
  //   nav ul ul li {
  //     border-bottom: 0px;
  //   }
  //   span.cancel:before {
  //     content: "\f00d";
  //   }
  // }
  // .content {
  //   z-index: -1;
  //   position: absolute;
  //   top: 50%;
  //   left: 50%;
  //   transform: translate(-50%, -50%);
  //   text-align: center;
  // }
  // header {
  //   font-size: 35px;
  //   font-weight: 600;
  //   padding: 10px 0;
  // }
  // p {
  //   font-size: 30px;
  //   font-weight: 500;
  // }
`;
