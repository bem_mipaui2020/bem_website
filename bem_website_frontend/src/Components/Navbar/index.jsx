import React, { Fragment, useState, useEffect } from "react";
import axios from "axios";
import Logo from "./Media/logo.png";
import ReorderIcon from "@material-ui/icons/Reorder";
import { Wrapper, Flex } from "./styles";
import { sessionService } from "redux-react-session";
import { headers, baseUrl } from "../../config";

const Navbar = () => {
  const token = localStorage.getItem("token");

  const handleLogout = () => {
    axios.get(`${baseUrl}/logout`, headers()).then((res) => {
      console.log(res);
      console.log(res.data);

      localStorage.removeItem("token");
      window.location.reload();
    });
  };

  const checkAuth = () => {
    if (token === null) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <Fragment>
      <Wrapper>
        <nav>
          <div class="logo">
            <img src={Logo} alt="Logo-Mipa" href="/" />
          </div>
          <label for="btn" class="icon">
            <ReorderIcon />
          </label>
          <input type="checkbox" id="btn" />
          <ul>
            <li>
              <a href="/">Selasar</a>
            </li>
            <li>
              <a href="/tentang-kami">Tentang Kami</a>
            </li>
            <li>
              <a href="/adkesma">Adkesma</a>
            </li>
            <li>
              <label for="btn-1" class="show">
                Artikel +
              </label>
              <a href="#">Artikel</a>
              <input type="checkbox" id="btn-1" />
              <ul>
                <li>
                  <a href="/artikel">Artikel 1</a>
                </li>
                <li>
                  <a href="/artikel2">Artikel 2</a>
                </li>
                <li>
                  <a href="/artikel3">Artikel 3</a>
                </li>
              </ul>
            </li>
            {token !== null && (
              <Fragment>
                <li>
                  <label for="btn-1" class="show">
                    Management +
                  </label>
                  <a href="#">Management</a>
                  <input type="checkbox" id="btn-1" />
                  <ul>
                    <li>
                      <a href="/adkesma-management">Adkesma</a>
                    </li>
                    <li>
                      <a href="/artikel-management">Artikel</a>
                    </li>
                  </ul>
                </li>
              </Fragment>
            )}
            <li>
              <label for="btn-2" class="show">
                Keluarga Kami +
              </label>
              <a href="#">Keluarga Kami</a>
              <input type="checkbox" id="btn-2" />
              <ul>
                <li>
                  <Flex direction="row">
                    <a href="/keluarga-kami" style={{ lineHeight: 1.4 }}>
                      Pengurus Inti
                    </a>
                  </Flex>
                </li>

                <li>
                  <Flex direction="row">
                    <a href="/anggota/zaki" style={{ lineHeight: 1.4 }}>
                      Satuan Pengendali Internal
                    </a>
                  </Flex>
                </li>

                <li>
                  <label for="btn-1" class="show">
                    Departemen +
                  </label>
                  <Flex direction="row">
                    <a href="#" style={{ lineHeight: 1.4 }}>
                      Departemen
                    </a>
                  </Flex>

                  <input type="checkbox" id="btn-1" />
                  <ul>
                    <li>
                      <a href="/anggota/departemen-keilmuan">Keilmuan</a>
                    </li>
                    <li>
                      <a href="/anggota/departemen-senbud">Seni dan Budaya</a>
                    </li>
                    <li>
                      <a href="/anggota/departemen-olahraga">Olahraga</a>
                    </li>
                    <li>
                      <a href="/anggota/departemen-adkesma">Adkesma</a>
                    </li>
                    <li>
                      <Flex direction="row">
                        <a href="/anggota/departemen-pemnas" style={{lineHeight: 1.5}}>
                          Pembinaan dan Kaderasisasi
                        </a>
                      </Flex>
                    </li>
                    <li>
                      <Flex direction="row">
                        <a href="/anggota/departemen-sosling" style={{lineHeight: 1.5}}>Sosial dan Lingkungan</a>
                      </Flex>
                    </li>
                    <li>
                      <a href="/anggota/departemen-kastrat">Kastrat</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <label for="btn-1" class="show">
                    Biro +
                  </label>

                  <a href="#">Biro</a>

                  <input type="checkbox" id="btn-1" />
                  <ul>
                    <li>
                      <a href="/adkesma-management">Kewirausahaan</a>
                    </li>
                    <li>
                      <a href="/artikel-management">Keskretaritan</a>
                    </li>
                    <li>
                      <Flex direction="row">
                        <a href="/anggota/zaki" style={{ lineHeight: 1.4 }}>
                          Pengembangan dan Sumber Daya Manusia
                        </a>
                      </Flex>
                    </li>

                    <li>
                      <Flex direction="row">
                        <a href="/anggota/zaki" style={{ lineHeight: 1.4 }}>
                          Penelitian dan Pengembangan
                        </a>
                      </Flex>
                    </li>
                    <li>
                      <Flex direction="row">
                        <a
                          href="/artikel-management"
                          style={{ lineHeight: 1.5 }}
                        >
                          Hubungan Masyarakat
                        </a>
                      </Flex>
                    </li>
                    <li>
                      <a href="/artikel-management">Multimedia</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>

            <li>
              {checkAuth() === true ? (
                <a href="/login">Login</a>
              ) : (
                <a onClick={handleLogout}>Logout</a>
              )}
            </li>
          </ul>
        </nav>
      </Wrapper>
    </Fragment>
  );
};

export default Navbar;
