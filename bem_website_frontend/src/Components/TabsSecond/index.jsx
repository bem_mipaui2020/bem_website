import React from 'react';
import PropTypes from 'prop-types';
import {Button, Paper, Grid} from "@material-ui/core"
import {Wrapper, Flex} from "./styles"
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import SwipeableViews from 'react-swipeable-views';

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-auto-tabpanel-${index}`}
        aria-labelledby={`scrollable-auto-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

  function a11yProps(index) {
    return {
      id: `scrollable-auto-tab-${index}`,
      'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
  }
  
    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
            width: '100%',
            backgroundColor: theme.palette.background.paper,
        },
        text:{
            color: "black",
            textTransform: "Capitalize"
        }
    }));

// const useStyles = makeStyles((theme) => ({
//     root: {
//     // flexGrow: 1,
//     // width: 300,
//     // backgroundColor: theme.palette.background.paper,
//     },
//     app:{
//         background: "white"
//     },
//     text:{
//         color: "black",
//         textTransform: "Capitalize"
//     }
// }));

export default function SimpleTabs() {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleChangeIndex = (index) => {
    setValue(index);
  };


    const multimedia = [
        {title: "Pemenuhan Kebutuhan Publikasi", value: "Pembuatan media penunjang kegiatan publikasi visual berupa poster cetak dan non-cetak, baliho, banner, video, dan lain sebagainya"},
        {title: "Kelas Desain", value: "Pengadan workshop desain, berkolaborasi bersama Biro Multimedia atau Biro Hubungan Masyarakat dari Himpunan Mahasiswa Departemen yang ada di FMIPA UI"},

    ]
    const adkesma = [
        {title: "Media dan Informasi (MED IN ADKESMA)", value: "Wadah dalam pemberian informasi dan pelayanan kepada mahasiswa FMIPA UI mencakup akademik, biaya pendidikan, beasiswa, magang dan seputar pencerdasan program kerja yang dimiliki Adkesma."},
        {title: "Advokasi Beasiswa", value: "Upaya untuk memberikan pelayanan terhadap mahasiswa FMIPA UI yang berkaitan dengan beasiswa."},
        {title: "Daun MIPA", value: "Media peminjaman uang terkait kebutuhan mendesak seperti BOP."},
        {title: "Advokasi BOP", value: "Fasilitator bagi mahasiswa FMIPA UI dalam pengajuan update dan cicilan BOP agar tidak ada permasalahan biaya pendidikan"},
        {title: "Advokasi Fasilitas", value: "Fasilitator untuk mengadvokasi terkait fasilitas yang ada di FMIPA UI"},
        {title: "DATIN Kesma", value: "Database Mahasiswa FMIPA UI dengan bantuan Kesma HMD"},
        {title: "MIPA ASPIRATION CENTER", value: "Wadah aspirasi bagi mahasiswa FMIPA UI yang ingin menyampaikan aspirasinya kepada pihak terkait/pimpinan fakultas"},
        {title: "NGOBAT (Ngobrol Bareng Dekanat)", value: "Ruang diskusi bersama dekanat sebagai wadah aspirasi mahasiswa FMIPA UI untuk secara langsung menyampaikan aspirasi ke pihak terkait"},
        {title: "Kumpul Kesma MIPA (KK MIPA)", value: "Kegiatan diskusi rutin Adkesma FMIPA UI dengan Kesma HMD dalam pendistribusian informasi dan mencapai kesepakatan bersama dalam menyelesaikan masalah."},
        {title: "MIPA BERCERITA", value: "Program kerja terkait kesehatan mental yang disertai dengan sarana berupa akun resmi instagram @mipabercerita yang dibentuk oleh Adkesma BEM FMIPA UI 2020 untuk menghasilkan kesadaran dan membantu mengatasi perihal isu kesehatan mental."},

    ]
    const pembinaan = [
        {title: "Form Evaluasi Pembinaan", value: "Evaluasi setiap program kerja Departemen Pembinaan dan Kaderisasi BEM FMIPA UI 2020 setelah selesai dalam bentuk form penilaian responden"},
        {title: "Lembar Kaderisasi Berkelanjutan (LKB)", value: "Lembar penilaian performa staf selama satu tahun kepengurusan"},
        {title: "Pengenalan Sistem Akademik Fakultas (PSAF)", value: "Kegiatan rangkaian pengenalan sistem akademik fakultas untuk mahasiswa baru dan sekaligus merupakan wadah pembinaan fase I"},
        {title: "MIPA Leadership Development Program (MLDP)", value: "Wadah bagi seluruh Mahasiswa FMIPA UI untuk mendapat pengetahuan terkait pengembangan soft skill kepemimpinan"},
        {title: "Sekolah Kaderisasi Lembaga (SKL)", value: "Wadah untuk mempersiapkan kader yang akan meneruskan estafet kepengurusan di lembaga tingkat fakultas"},
        {title: "PSAF Merangkai Makna", value: "Forum diskusi BEM FMIPA UI dengan lembaga-lembaga lainnya untuk merumuskan nilai pembinaan yang akan dibawa pada PSAF"},

    ]
    const sosial = [
        {title: "MGV (MIPA Green Village)", value: "Wadah untuk berkontribusi dalam ranah pengabdian masyarakat di FMIPA UI yang selama ini berfokus pada multi aspek sosial, yaitu kesehatan lingkungan, ekonomi, dan  pendidikan. "},
        {title: "Teras Belajar", value: "Wadah untuk berkontribusi dalam ranah pengabdian masyarakat di FMIPA UI yang selama ini berfokus pada multi aspek sosial, yaitu kesehatan lingkungan, ekonomi, dan  pendidikan. "},
        {title: "Kajian Sosial dan Lingkungan", value: "Wujud fungsi Departemen Sosial dan Lingkunga untuk menyelesaikan secara perlahan masalah kebersihan di FMIPA UI yang menitik beratkan pada kebersihan dallas dan pengelolaan sampah FMIPA."},
        {title: "Media dan Propaganda", value: "Wujud fungsi Departemen Sosial dan Lingkungan dalam kaitannya untuk melakukan perayaan hari-hari besar sosial & lingkungan hidup secara online dan offline dan juga untuk melakukan publikasi mengenai berita-berita seputar isu sosial & lingkungan yang ada"},
        {title: "MIPA Peduli ", value: "Salah satu fungsi dari Departemen Sosial dan Lingkungan BEM FMIPA UI 2020 yang merupakan platform bagi masyarakat FMIPA UI untuk berdonasi"},

    ]
    const kajian = [
        {title: "Forkoti++", value: "Forum diskusi ketua lembaga se-MIPA dengan Kepala Departemen Kastrat BEM FMIPA UI yang diinisiasi oleh Departemen Kastrat BEM FMIPA UI 2020"},
        {title: "Konsolidasi", value: "Forum antara Departemen Kastrat BEM FMIPA dengan satu atau lebih lembaga yang membahas arah pergerakan untuk mencapai tujuan fokus isu"},
        {title: "Kajian Isu", value: "Program kerja yang mengasilkan tulisan mengenai permasalahan beserta rekomendasi solusi dengan rancangan advokasi yang jelas berdasarkan moral dan intelektual untuk melandasi suatu aksi dan memiliki keberpihakan pada lingkungan dan/atau rakyat"},
        {title: "Seminar Daring", value: "Kegiatan yang bersifat formal dengan menghadirkan narasumber berkompeten dan ahli untuk membahas dan mengkaji isu utama BEM FMIPA UI 2020. Peserta seminar daring boleh menanggapi pemaparan dari narasumber sehingga terjadi diskusi. Program Kerja ini sebagai pengganti Diskusi Publik, Mimbar Bebas, dan Disandal."},
        {title: "Ekstraksi", value: "Wadah kaderisasi gerakan sosial politik yang ditujukan untuk mahasiswa baru FMIPA UI sekaligus penanaman nilai pergerakan dan pengabdian"},
        {title: "Sarapan Kornet", value: "Akronim dari “Sarapan Koran Internet” yaitu tulisan terkait isu sedang hangat dibicarakan selama sepekan. Tulisan yang membahas isu hangat selama sepekan kemudian dipublikasikan di media sosial BEM FMIPA UI. Hasil publikasi tersebut memuat konten (infografis, video, meme atau lainny1. inti substansi dan tautan yang memuat tulisan Sarapan Koran Internet tersebut"},
        {title: "Protein", value: "Peringatan Hari Besar"},
        {title: "Peringatan Hari Besar", value: "Propaganda darat dan/atau udara untuk memeringati Hari Besar tertentu.  Peringatan Hari Besar merupakan suatu upaya untuk mengingatkan publik terhadap suatu peristiwa sejarah tertentu. Peristiwa yang akan diperingati disesuaikan dengan momentum. Target pelaksanaannya adalah mengingatkan apa yang belum tuntas, apa yang perlu diperbaiki, dan apa yang perlu dilakukan. "},
    ]
 

  return (
    <Wrapper>
        

    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
            <Tab className={classes.text} label="Multimedia" {...a11yProps(0)}  />
            <Tab className={classes.text} label="Adkesma" {...a11yProps(1)} />
            <Tab className={classes.text} label="pembinaan dan kaderisasi" {...a11yProps(2)} />
            <Tab className={classes.text} label="Sosial dan Lingkungan" {...a11yProps(3)} />
            <Tab className={classes.text} label="Kajian dan Aksi Strategis" {...a11yProps(4)} />

            
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
            <Flex directionn="row" justify="center" alignItems="center" wrap="wrap" style={{width: '60em'}}>
                {multimedia.map((item) => (
                    <Paper className="paper">
                        <Flex direction="row" justify="center" className="header">
                            <h3>{item.title}</h3>
                        </Flex>
                    <p className="content">{item.value} </p>
                    </Paper>
                ))}
            </Flex>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Flex direction="row" justify="center" alignItems="center" wrap="wrap" style={{width: '60em'}}>
            {adkesma.map((item) => (
                <Paper className="paper">
                    <Flex direction="row" justify="center" className="header">
                        <h3>{item.title}</h3>
                    </Flex>
                    <p className="content">{item.value} </p>
                </Paper>
            ))}
        </Flex>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Flex direction="row" justify="center" alignItems="center" wrap="wrap" style={{width: '60em'}}>
            {pembinaan.map((item) => (
                <Paper className="paper">
                    <Flex direction="row" justify="center" className="header">
                        <h3>{item.title}</h3>
                    </Flex>
                    <p className="content">{item.value} </p>
                </Paper>
            ))}
        </Flex>
      </TabPanel>
      <TabPanel value={value} index={3}>
        <Flex direction="row" justify="center" alignItems="center" wrap="wrap" style={{width: '60em'}}>
            {sosial.map((item) => (
                <Paper className="paper">
                    <Flex direction="row" justify="center" className="header">
                        <h3>{item.title}</h3>
                    </Flex>
                    <p className="content">{item.value} </p>
                </Paper>
            ))}
        </Flex>
      </TabPanel>
      <TabPanel value={value} index={4}>
        <Flex direction="row" justify="center" alignItems="center" wrap="wrap" style={{width: '60em'}}>
            {kajian.map((item) => (
                <Paper className="paper">
                    <Flex direction="row" justify="center" className="header">
                        <h3>{item.title}</h3>
                    </Flex>
                    <p className="content">{item.value} </p>
                </Paper>
            ))}
        </Flex>
      </TabPanel>
    </div>


    

    
    </Wrapper>
  );
}
