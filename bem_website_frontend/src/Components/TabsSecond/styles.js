import styled from "styled-components"

export const Flex = styled.div`
    display: flex;
    flex-direction: ${({direction}) => direction};
    justify-content: ${({justify}) => justify};
    align-items: ${({alignItems}) => alignItems};
    flex-wrap: ${({wrap}) => wrap};
`

export const Wrapper = styled.div`
    color: black;

    .paper{
        width: 12em;
        height: 16em;
        margin-top: 10px;
        margin-left: 25px;
    }
    .header{
        width: 100%;
        height: 70px;
        padding-bottom: 10px;
        background-color: #417288;
        color: white;
        text-align: center;
        border-radius: 5px 5px 0 0
    }
    .content{
        padding: 5px;
        text-align: center;
        font-size: 15px;
        line-height: 23px;
    }
`