import React from 'react';
import PropTypes from 'prop-types';
import {Button, Paper, Grid} from "@material-ui/core"
import {Wrapper, Flex} from "./styles"
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import SwipeableViews from 'react-swipeable-views';

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-auto-tabpanel-${index}`}
        aria-labelledby={`scrollable-auto-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

  function a11yProps(index) {
    return {
      id: `scrollable-auto-tab-${index}`,
      'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
  }
  
    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
            width: '100%',
            backgroundColor: theme.palette.background.paper,
        },
        text:{
            color: "black",
            textTransform: "Capitalize"
        }
    }));

// const useStyles = makeStyles((theme) => ({
//     root: {
//     // flexGrow: 1,
//     // width: 300,
//     // backgroundColor: theme.palette.background.paper,
//     },
//     app:{
//         background: "white"
//     },
//     text:{
//         color: "black",
//         textTransform: "Capitalize"
//     }
// }));

export default function SimpleTabs() {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleChangeIndex = (index) => {
    setValue(index);
  };


    const keilmuan = [
        {title: "Seleksi Mahasiswa Berprestasi", value: "Sebuah kompetisi mahasiswa berprestasi pada bidang keilmuan dan kategori di tingkat departemen, fakultas, dan universitas"},
        {title: "Olimpiade Ilmiah Mahasiswa FMIPA UI (OIM MIPA)", value: "Sebuah ajang kompetisi di bidang keilmuan yang terdiri dari beberapa cabnag lomba yaitu esai, PKM, Debat Bahasa Indonesia, Debat Bahasa Inggris, OIM Quiz, dan poster antar departemen"},
        {title: "MIPA Towards OIM UI (MTO UI)", value: "Sebuah program pelatihan dan persiapan kontingen FMIPA UI untuk berkompetisi dalam OIM UI."},
        {title: "Grafinity", value: "Komunitas yang berfungsi untuk menuansakan, mewadahkan, dan meningkatkan minat dan bakat mahasiswa FMIPA UI dalam bidang keilmuan."},
        {title: "Mipa Apresiasi", value: "Memberikan Apresiasi terhadap mahasiswa FMIPA UI yang berprestasi di bidang Keilmuan di dalam berbagai lomba, baik lomba Dikti atau non-Dikti melalui publikasi prestasi di media sosial."},
        {title: "Comic Sa(i)ns", value: "Pemberian informasi yang menarik seputar sains yang dikemas lebih menarik dan informatif, agar terbentuknya pemikiran bahwa sains itu menyenangkan dan tidak selalu membosankan."},
        {title: "Lomba Dari Rumah", value: "Lomba esai dan poster infografis nasional"},

    ]
    const senbud = [
        {title: "RejuvenARTion", value: "Pembentukan kembali klub artist (art scientist) yang terdiri dari Viva, Density, Theatrist, Scienema, dan Kogami"},
        {title: "Artshow", value: "Pementasan seni para seniman FMIPA UI (Artist)."},
        {title: "Dallas Nyeni", value: "Program pemberi suasana seni yang dikemas secara menarik, kreatif, dan elegan."},
        {title: "Art of the Month", value: "Program pengumpulan karya seni mahasiswa FMIPA UI"},
        {title: "CelebrARTion", value: "Peringatan dan perayaan hari seni dan budaya nasional."},
        {title: "Sendalink & ARTlink", value: "Rapat guna menghubungkan antara Dept. Senbud BEM FMIPA UI dengan Dept. Senbud HMD dan juga dengan komunitas seni yang ada di FMIPA UI."},
        {title: "Pekan Seni Tiga", value: "Lomba dan festival seni FMIPA UI."},
        {title: "MIPA TOWARDS UIAW 2020", value: "Program mempersiapkan mahasiswa FMIPA UI dalam UI Art War 2020 mulai dari pemilihan ketua kontingen, serta pemilihan dan manajemen persiapan kontingen itu sendiri."},

    ]
    const olahraga = [
        {title: "Latihan Rutin", value: "Program yang diadakan untuk mahasiswa FMIPA UI yang ingin mengembangkan minat dan bakatnya di dalam Unit Kegiatan dan Komunitas Olahraga FMIPA UI"},
        {title: "Ksatria MIPA", value: "Bentuk kerja sama antara Komunitas dan Unit Kegiatan Olahraga untuk mendata minat dan bakat mahasiswa aktif FMIPA UI"},
        {title: "Ksatria Bersatu", value: "Rapat koordinasi antara Departemen Olahraga BEM FMIPA UI 2020 dengan Departemen Olahraga Himpunan dan Unit Kegiatan Olahraga di FMIPA UI"},
        {title: "Sehat Bareng Depor", value: "Program kerja Departemen Olahraga yang berbasis daring dengan tujuan untuk mengajak seluruh masyarakat FMIPA UI olahraga Bersama melalui daring selama pandemi Covid-19. "},
        {title: "MIPA CUP", value: "Ajang kompetisi olahraga antar departemen sebagai bentuk persiapan menuju Olimpiade UI dan hiburan bagi mahasiswa FMIPA UI."},
        {title: "Fun Sports Event", value: "Program insidental yang bertujuan untuk meningkatkan suasana olahraga dan menjalin silahturahmi dengan seluruh masyarakat FMIPA UI."},
        {title: "Volley Competition (VOLCOM)", value: "Ajang kompetisi bola voli antar fakultas di UI yang diadakan oleh FMIPA UI guna sebagai sparring antarfakultas dan mengetahui kebehatan fakultasn lain di UI"},
        {title: "MIPA TOWARDS OLIM 2020", value: "Program mempersiapkan Ksatria MIPA dalam ajang Olim UI 2020 dimulai dari pemilihan struktur kepengurusan MTO, serta persiapan kontingen untuk menuju Olim UI"},

    ]
 

  return (
    <Wrapper>
        

        <div className={classes.root}>
        <AppBar position="static" color="default">
            <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
            aria-label="scrollable auto tabs example"
            >
                <Tab className={classes.text} label="keilmuan" {...a11yProps(0)}  />
                <Tab className={classes.text} label="seni dan budaya" {...a11yProps(1)} />
                <Tab className={classes.text} label="olahraga" {...a11yProps(2)} />
            </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
                <Flex directionn="row" justify="center" alignItems="center" wrap="wrap" style={{width: '60em'}}>
                    {keilmuan.map((item) => (
                        <Paper className="paper">
                            <Flex direction="row" justify="center" className="header">
                                <h3>{item.title}</h3>
                            </Flex>
                        <p className="content">{item.value} </p>
                        </Paper>
                    ))}
                </Flex>
        </TabPanel>
        <TabPanel value={value} index={1}>
            <Flex direction="row" justify="center" alignItems="center" wrap="wrap" style={{width: '60em'}}>
                {senbud.map((item) => (
                    <Paper className="paper">
                        <Flex direction="row" justify="center" className="header">
                            <h3>{item.title}</h3>
                        </Flex>
                        <p className="content">{item.value} </p>
                    </Paper>
                ))}
            </Flex>
        </TabPanel>
        <TabPanel value={value} index={2}>
            <Flex direction="row" justify="center" alignItems="center" wrap="wrap" style={{width: '60em'}}>
                {olahraga.map((item) => (
                    <Paper className="paper">
                        <Flex direction="row" justify="center" className="header">
                            <h3>{item.title}</h3>
                        </Flex>
                        <p className="content">{item.value} </p>
                    </Paper>
                ))}
            </Flex>
        </TabPanel>
        </div> 
    </Wrapper>
  );
}
