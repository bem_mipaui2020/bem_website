import React, { Fragment } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Login from "./Pages/Login";
import Homepage from "./Pages/Homepage";
import AboutUs from "./Pages/AboutUs";
import OurFamily from "./Pages/Family";
import Artikel1 from "./Pages/Article/Article1";
import Artikel2 from "./Pages/Article/Article2";
import Artikel3 from "./Pages/Article/Article3";
import ArticleManagement from "./Pages/ArticleManagement";

import Anggota from "./Pages/Anggota";
import Zaki from "./Pages/Anggota/Zaki";
import Pramediya from "./Pages/Anggota/Pramediya";
import Farah from "./Pages/Anggota/Farah";
import Yoshua from "./Pages/Anggota/Yoshua";
import Annisaa from "./Pages/Anggota/Annisaa";
import Triananda from "./Pages/Anggota/Triananda";
import Ginanjar from "./Pages/Anggota/Ginanjar";
import Arif from "./Pages/Anggota/Arif";

import DepAdkesma from "./Pages/Anggota/Departemen/Adkesma";
import DepKastrat from "./Pages/Anggota/Departemen/Kastrat";
import DepKeilmuan from "./Pages/Anggota/Departemen/Keilmuan";
import DepOlahraga from "./Pages/Anggota/Departemen/Olahraga";
import DepPemnas from "./Pages/Anggota/Departemen/Pemnas";
import DepSenbud from "./Pages/Anggota/Departemen/Senbud";
import DepSosling from "./Pages/Anggota/Departemen/Sosling";

import Detail from "./Pages/DetailArticle";
import CreateArticle from "./Pages/CreateArticle";
import Adkesma from "./Pages/Adkesma";
import AdkesmaManagement from "./Pages/AdkesmaManagement";

const Router = () => {
  return (
    <Fragment>
      <BrowserRouter>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/" component={Homepage} />
          <Route exact path="/tentang-kami" component={AboutUs} />
          <Route exact path="/keluarga-kami" component={OurFamily} />
          <Route exact path="/artikel" component={Artikel1} />
          <Route exact path="/artikel2" component={Artikel2} />
          <Route exact path="/artikel3" component={Artikel3} />
          <Route
            exact
            path="/artikel-management"
            component={ArticleManagement}
          />
          <Route exact path="/create-article" component={CreateArticle} />
          <Route exact path="/adkesma" component={Adkesma} />
          <Route
            exact
            path="/adkesma-management"
            component={AdkesmaManagement}
          />

          <Route exact path="/anggota" component={Anggota} />
          <Route exact path="/anggota/zaki" component={Zaki} />
          <Route exact path="/anggota/farah" component={Farah} />
          <Route exact path="/anggota/yoshua" component={Yoshua} />
          <Route exact path="/anggota/annisaa" component={Annisaa} />
          <Route exact path="/anggota/triananda" component={Triananda} />
          <Route exact path="/anggota/ginanjar" component={Ginanjar} />
          <Route exact path="/anggota/arif" component={Arif} />
          <Route exact path="/anggota/pramediya" component={Pramediya} />

          <Route
            exact
            path="/anggota/departemen-adkesma"
            component={DepAdkesma}
          />
          <Route
            exact
            path="/anggota/departemen-kastrat"
            component={DepKastrat}
          />
          <Route
            exact
            path="/anggota/departemen-keilmuan"
            component={DepKeilmuan}
          />
          <Route
            exact
            path="/anggota/departemen-olahraga"
            component={DepOlahraga}
          />
          <Route
            exact
            path="/anggota/departemen-pemnas"
            component={DepPemnas}
          />
          <Route
            exact
            path="/anggota/departemen-senbud"
            component={DepSenbud}
          />
          <Route
            exact
            path="/anggota/departemen-sosling"
            component={DepSosling}
          />

          <Route
            exact
            path="/detail"
            render={(props) => <Detail {...props} />}
          />
        </Switch>
      </BrowserRouter>
    </Fragment>
  );
};

export default Router;
