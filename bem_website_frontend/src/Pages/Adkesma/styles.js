import styled from "styled-components"

export const Flex = styled.div`
    display: flex;
    flex-direction: ${({direction}) => direction};
    justify-content: ${({justify}) => justify};
    align-items: ${({alignItems}) => alignItems};
    flex-wrap: ${({wrap}) => wrap};
`

export const Post = styled.div`
    margin-top: 50px;
    text-decoration: none;

`

export const Title = styled.div`
    text-align: center;
    font-size: 35px;
    margin-top: 30px;
    
`

export const Card = styled.div`
    margin-top: 20px;
    width: 400px;
    height: 50px;
    padding: 15px;
    // padding-left: 10px;
    margin-bottom: 1rem;
    border: 1px solid black;
    border-left: 4px solid #417288;
    :hover{
        background: #417288;
        transition: 0.5s;
        color: white;
        cursor: pointer;
    }
    
`