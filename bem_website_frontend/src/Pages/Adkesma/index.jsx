import React, { Fragment, useState, useEffect } from "react";
import axios from "axios";
import { Post, Header, Card, Title, Flex } from "./styles";
import { CircularProgress, Link } from "@material-ui/core";
import Navbar from "../../Components/Navbar";
import { headers, baseUrl } from "../../config";

const Adkesma = () => {
  function headers() {
    const token = localStorage.getItem("token");
    return {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
  }
  // var baseUrl = "http://localhost:8000/api"
  const [items, setItems] = useState([]);
  useEffect(() => {
    axios.get(`${baseUrl}/adkesma`, headers()).then((res) => {
      const items = res.data.data;
      console.log(res);
      setItems(items);
    });
  }, []);
  console.log(items);

  return (
    <Fragment>
      <Navbar />
      <Title>Suara Adkesma</Title>
      <Post>
        {items.length === 0 ? (
          <Flex direction="row" justify="center">
            <CircularProgress />
          </Flex>
        ) : (
          <Flex direction="row" justify="space-around" wrap="wrap">
            {items.map((items) => (
              <Link href={items.url} style={{ textDecoration: "none" }}>
                <Card>
                  <p>{items.caption}</p>
                </Card>
              </Link>
            ))}
          </Flex>
        )}
      </Post>
    </Fragment>
  );
};

export default Adkesma;
