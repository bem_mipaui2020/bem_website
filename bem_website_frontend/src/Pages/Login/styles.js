import styled from "styled-components"

export const Flex = styled.div`
    display: flex;
    flex-direction: ${({direction}) => direction};
    justify-content: ${({justify}) => justify};
    align-items: ${({alignItems}) => alignItems};
    flex-wrap: ${({wrap}) => wrap};
`

export const Wrapper = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    position: fixed;
    justify-content: center;
    align-items: center;
    background-color: #417288;

`
export const Left = styled.div`
    width: 508px;
    height: 599px;
    background-color: #1D252D;
    border-radius: 20px 0 0 20px;
    h1{
        padding: 30px;
        color: white;
    }
    @media (max-width: 500px){
        width: 360px;
        border-radius: 20px;
    }
    a{
        margin-top: 100px;
        text-decoration: none;
        color: white;
        font-size: 16px;
        text-align: center;
        &:hover{
            background: #BB4628;
        }
    }
`

export const Right = styled.div`
    width: 508px;
    height: 599px;
    background-color: white;
    border-radius: 0 20px 20px 0;
    img{
        
        padding-top: 100px;
        padding-left: 80px;
        width: 370px;
        height: 370px;
    }
    @media (max-width: 500px){
        display: none;
    }
`