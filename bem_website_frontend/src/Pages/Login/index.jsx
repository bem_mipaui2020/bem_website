import React, { Fragment, useState } from "react";
import axios from 'axios'
import { sessionService } from 'redux-react-session';
import { Button, Paper, Grid, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Wrapper, Flex, Left, Right } from "./styles";
import Makara from "./Media/Makara.png";
import {headers, baseUrl} from '../../config'
import { Redirect, Link, useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      width: "35ch",
      color: "white",
    },
    "& .MuiInput-underline:before": {
      borderBottomColor: "white",
      "&:hover": {
        borderBottomColor: "yellow",
      },
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#BB4628",
    },
    "& label.Mui-focused": {
      color: "white",
    },
  },
  name: {
    "& > *": {
      color: "white",
    },
  },
}));

const Login = () => {
  const history = useHistory()
  const classes = useStyles();
  const token = localStorage.getItem("token");

  const [value, setValue] = useState({
    username: '',
    password: ''
  })
  const [isShowed, setIsShowed] = useState(false)
  const [loading, setLoading] = useState(false)

  const handelChange = (event) => {
    const values = event.target.value
      setValue({
          ...value,
          [event.target.name]: values
      })
      console.log(values)
  }

  const handlePost = () => {
    
    setLoading(true)
    const user = {
      username: value.username,
      password: value.password
    }
    axios.post(`${baseUrl}/login`, user)
      .then(res => {
        console.log(res)
        console.log(res.data)

        if(res.data.auth === false){
          setValue({
            username: '',
            password: ''
          })
          setLoading(false)
          setIsShowed(true)
        }
        else{
          
          const {token} = res.data
          localStorage.setItem('token', token)
          history.push('/')
        }
    })
  }


  return (
    <Fragment>
      {
        token!==null?
        <Redirect to="/" />
        :
        <Wrapper>
        <Left>
          <h1>Member Login</h1>
          <Grid
            container
            direction="column"
            justify="flex-start"
            alignItems="flex-start"
            style={{ paddingLeft: "30px" }}
          >
            <TextField
              id="standard-basic"
              label="Username"
              name="username"
              value={value.username}
              className={classes.root}
              onChange={handelChange}
            />
            <TextField
              id="standard-basic"
              label="Password"
              type="password"
              name="password"
              value={value.password}
              className={classes.root}
              onChange={handelChange}
              style={{ marginTop: "30px" }}
            />
          </Grid>
          <Flex direction="row">
            <Flex direction="column"  style={{marginLeft: '30px'}}>
              <Button
                variant="contained"
                color="primary"
                style={{ marginTop: "30px" }}
                onClick={handlePost}
              >
                Login
              </Button>
            </Flex>
            {/* <Flex direction="column"  style={{marginRight: '30px'}}>
              <a href="#">Forgot Password ?</a>
            </Flex> */}
          </Flex>
          
          <Flex direction="row" style={{padding: '20px', marginLeft:'1em'}}>
              {isShowed ? 
                <p style={{color: '#BB4628', fontWeight: 'bold'}}>Invalid Username or Password!</p>
                :
                null
              }
            </Flex>
          <Flex direction='row' justify="center" style={{marginTop: '5rem'}}>
            <p style={{color: 'white'}}>BEM MIPA UI 2020</p>
          </Flex>
        </Left>
        <Right>
          <img src={Makara} alt="makara-mipa" />
        </Right>
      </Wrapper>
      }
      
    </Fragment>
  );
};

export default Login;
