import styled from "styled-components";
import About from "./Media/About.png";

export const Flex = styled.div`
  display: flex;
  flex-direction: ${({ direction }) => direction};
  justify-content: ${({ justify }) => justify};
  align-items: ${({ alignItems }) => alignItems};
  flex-wrap: ${({ wrap }) => wrap};
`;

export const Wrapper = styled.div`
  text-align: center;
  width: 100%;
  .lala {
    background-image: url("https://image.shutterstock.com/image-photo/wide-angle-panorama-autumn-forestmisty-260nw-1195159864.jpg");
  }
  img {
    width: 100%;
  }
  h1 {
    color: white;
    position: absolute;
    font-size: 48px;
    height: 76px;
    left: 150px;
    top: 300px;
  }
  h3 {
    color: white;
    position: absolute;
    font-size: 24px;
    height: 145px;
    left: 160px;
    top: 390px;
  }
`;

export const Square = styled.div`
  margin-top: 40px;
  widht: 800px;
  height: 200px;
  background-color: white;
  padding: 15px;

  .title {
    font-size: 60px;
    font-weight: bold;
  }
  .text {
    font-size: 20px;
    line-height: 100%;
  }
`;

export const Vision = styled.div`
  margin-top: 180px;
  img {
    width: 600px;
    height: 500px;
    @media (max-width: 500px) {
      margin-top: 100px;
      width: 100%;
    }
  }

  // width: 100%;
  // height: auto;
  // background-color: #417288;
  // padding: 40px;
  // h1{
  //     color: #E0A256;
  //     padding-left: 20px;
  // }
  // h2{
  //     color: white;
  //     width: 80%;
  //     text-align: center;
  //     padding-left: 100px;
  // }
`;

export const Mision = styled.div`
  margin-top: 150px;
  h1 {
    margin-left: 2em;
    font-size: 60px;
  }
  .paper {
    padding: 10px;
    margin-top: 15px;
    :hover {
      box-shadow: 3px 3px 5px 6px #ccc;
    }
    cursor: pointer;
  }
`;

export const Value = styled.div`
  margin-top: 150px;
  width: 100%;
  height: 50em;
  // padding: 50px;
  // padding-bottom: 50px;
  background-color: #ede6de;

  h1 {
    padding-top: 1em;
    margin-left: 2em;
    font-size: 60px;
    color: white;
  }
  .paper {
    width: 750px;
    height: 150px;
    margin-top: 15px;
    margin-left: 20px;
    :hover {
      box-shadow: 3px 3px 5px 6px #ccc;
    }
    border-left: 15px solid black;
    @media (max-width: 500px) {
      width: 450px;
      height: auto;
    }
  }
  .content {
    margin-left: 3em;
    margin-right: 3em;
    margin-top: 2em;
    margin-bottom: 2em;
    @media (max-width: 500px) {
      font-size: 14px;
    }
  }
  @media (max-width: 500px) {
    height: 70em;
  }
`;

export const Box = styled.div`
  margin-top: 40px;
  width: 100px;
  height: 80px;
  background-color: #1d252d;
  border-radius: 15px;
`;

export const Program = styled.div`
  margin-top: 150px;
  padding-left: 7em;
  padding-right: 7em;
  h1 {
    text-align: center;
  }

  // // display: flex;
  // margin-top: 100px;
  // // padding: 50px;
  // h1{
  //     color: #BB4628;
  // }
`;
