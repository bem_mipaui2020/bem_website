import React, { Fragment, useEffect } from "react";
import Aos from "aos";
import "aos/dist/aos.css";
import { Button, Paper, Grid } from "@material-ui/core";
import {
  Wrapper,
  Flex,
  Square,
  Vision,
  Mision,
  Value,
  Box,
  Program,
} from "./styles";
import Navbar from "../../Components/Navbar";
import Footer from "../../Components/Footer";
import About from "./Media/About.png";
import Tabs from "../../Components/Tabs";
import SecondTabs from "../../Components/TabsSecond";
import ThirdTabs from "../../Components/TabsThird";

const AboutUs = () => {
  useEffect(() => {
    Aos.init({ duration: 3000 });
  }, []);

  const nilai = {
    kolaboratif: `Dalam tatanan lembaga kemahasiswaan di FMIPA UI, kolaborasi merupakan hal yang esensial 
                  guna mengakomodasi kepentingan - kepentingan setiap elemen di FMIPA UI denngan lebih optimal. 
                  Dalam membawa gerakan, Badan Eksekutif Mahasiswa juga membutuhkan kolaborasi dengan mitra strategis
                  agar memiliki arah gerak yang lebih luas dan berkelanjutan`,
    konstruktif: `Konstruktif merupakan nilai yang dibawa oleh BEM FMIPA UI 2020 dengan empat harapan. 
                  Pertama, BEM FMIPA UI 2020 dapat membangun hubungan personal yang harmonis dan kinerja
                  yang efisien. Kedua, BEM FMIPA UI 2020 dapat mengakomodasi kepentingan warga FMIPA UI.
                  Ketiga, BEM FMIPA UI 2020 dapat memberikan manfaat dalam memajukan Universitas Indonesia.
                  Keempat BEM FMIPA UI 2020 dapat menjadi inisiator gerakan IKM FMIPA UI dalam memperjuangkan
                  kepentingan bangsa Indonesia.`,
    bermakna: `Segala yang terjadi dalam hidup pasti memiliki arti tersendiri. Oleh karena itu, penciptaan
                sebuah makna menjadi suatu hal yang perlu agar keluaran yang dihasilkan selaras dengan apa 
                yang diharapkan`,
  };

  return (
    <Fragment>
      <Navbar />
      <Wrapper>
        <h1>Tentang Kami</h1>
        <h3>
          We are founded by a leading academic and researcher in the field of
          Industrial Systems Engineering.
        </h3>
        <img src={About} alt="about-photo" />
      </Wrapper>
      <Vision data-aos="fade-left">
        <Flex direction="row" justify="center" wrap="wrap">
          <Flex direction="column" style={{ width: "35em" }}>
            <Square>
              <p className="title">Visi</p>
              <p className="text" style={{ lineHeight: 2 }}>
                BEM FMIPA UI 2020 yang kolaboratif dan Konstruktif dalam
                bergerak untuk merangkai makna bagi FMIPA UI, UI, dan Indonesia
              </p>
            </Square>
          </Flex>
          <Flex direction="column">
            <img
              src="https://www.redhat.com/cms/managed-files/developers-in-room-at-whiteboard.jpg"
              alt="developers in room at whiteboard"
            ></img>
          </Flex>
        </Flex>
      </Vision>

      <Mision data-aos="fade-up">
        <h1>Misi</h1>
        <Flex justifyy="row" justify="center" wrap="wrap">
          <Flex direction="column">
            <img
              src="https://i.pinimg.com/564x/92/e0/76/92e07603f43eff97af0414a41dda8e3c.jpg"
              alt="collage student"
              style={{ marginTop: "50px" }}
            />
          </Flex>
          <Flex
            direction="column"
            style={{ width: "35em", marginLeft: "100px" }}
          >
            <Paper className="paper">
              <p>
                <b>Membangun rasa kepemilikan</b> dalam internal kepengurusan
                BEM FMIPA UI 2020
              </p>
            </Paper>
            <Paper className="paper">
              <p>
                <b>Memberikan pelayanan </b>kepada warga FMIPA UI berdasarkan
                asas profesionalitas
              </p>
            </Paper>
            <Paper className="paper">
              <p>
                <b>Mengoptimalkan potensi, minat, dan bakat </b>mahasiswa FMIPA
                UI secara berkelanjutan
              </p>
            </Paper>

            <Paper className="paper">
              <p>
                <b>Membangun kolaborasi positif </b>dengan pihak eksternal untuk
                menciptakan kemanfaatan bagi FMIPA UI
              </p>
            </Paper>
            <Paper className="paper">
              <p>
                <b>Menjadi inisiator gerakan strategis </b>yang berdasarkan
                moral dan intelektual
              </p>
            </Paper>
          </Flex>
        </Flex>
      </Mision>

      <Value>
        <h1>Value</h1>
        <Flex
          direction="column"
          justify="center"
          alignItems="center"
          data-aos="fade-up"
        >
          <Flex direction="row" justify="space-around" wrap="wrap">
            <h2 style={{ marginTop: "3em" }}>Kolaboratif</h2>
            <Paper className="paper">
              <p className="content">{nilai.kolaboratif}</p>
            </Paper>
          </Flex>

          <Flex direction="row" justify="space-around" wrap="wrap">
            <h2 style={{ marginTop: "3em" }}>Konstruktif</h2>
            <Paper className="paper">
              <p className="content" style={{ marginTop: "1.3em" }}>
                {nilai.konstruktif}{" "}
              </p>
            </Paper>
          </Flex>
          <Flex direction="row" justify="space-around" wrap="wrap">
            <h2 style={{ marginTop: "3em" }}>Bermakna </h2>
            <Paper className="paper" style={{ marginLeft: "35px" }}>
              <p className="content" style={{ marginTop: "3em" }}>
                {nilai.bermakna}
              </p>
            </Paper>
          </Flex>
        </Flex>
      </Value>

      <Program>
        <Flex
          direction="row"
          justify="flex-start"
          style={{ paddingLeft: "3rem" }}
        >
          <h1>Program Kerja</h1>
        </Flex>
        <Flex direction="column" justify="center" alignItems="center">
          <Tabs />
          <SecondTabs />
          <ThirdTabs />
        </Flex>
        {/* <Grid container>
          <Grid container md={12}>
            <h1>Program Kerja</h1>
          </Grid>
          <Grid container md={9} style={{ marginLeft: "180px" }}>
            <Tabs />
          </Grid>
          <Grid container md={9} style={{ marginLeft: "180px" }}>
            <Tabs />
          </Grid>
        </Grid> */}
      </Program>
      <Footer />
    </Fragment>
  );
};

export default AboutUs;
