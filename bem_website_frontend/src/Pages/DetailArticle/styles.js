import styled from "styled-components"

export const Wrapper = styled.div`
    display: flex;
    width: 100vw;
    height: 100vh;
    justify-content: center;
    margin-top: 50px;
    
`

export const Title = styled.div`
    font-weight: 500;
    font-size: 48px;
    line-height: 56px;
`

export const Content = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-weight: normal;
    font-size: 20px;
    line-height: 28px;

    img{
        width: 100%;
    }
    button{
        width: 5em;
        height: 3em;
    }
`

export const Bottom = styled.div`
    display: flex;
    // width: 100vw;
    // height: 100vh;
    justify-content: center;
    margin-top: 40em;
    
`