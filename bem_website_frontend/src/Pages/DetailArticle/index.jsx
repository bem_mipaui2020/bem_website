import React, {Fragment, useEffect} from "react"
import {Grid, Button} from "@material-ui/core"
import {Wrapper, Title, Content, Bottom} from "./styles"
import Navbar from "../../Components/Navbar"
import Footer from "../../Components/Footer"
import {headers, baseUrl} from '../../config'

const Detail = () => {

    // const [items, setItems] = useState([])
    // const [id, setId] = useState('')
    // const handleClickOpen = (id) => {
    //     setId(id)
    // };
    // useEffect((props) => {
    //     // axios.get(`${baseUrl}/detail-article/${id}`)
    //     console.log("property_id",this.props.location.state.property_id)
    // },[])
    // componentDidMount(props);{
    //     console.log("property_id",this.props.location.state.property_id);}
    return(
        <Fragment>
            <Navbar />
            <Wrapper>
                <Grid container justify="center" direction="row">
                    <Grid item md={8}>
                        <Title>6 Crazy Details from Alphabet’s Leaked Plans for Its First Smart City</Title>
                        <Content>
                            <p>
                                Sidewalk Labs wants to rethink cities. We’re finally getting a look at what that will mean in Toronto, 
                                where the company is building a prototype neighborhood.
                            </p>
                            <img src="https://miro.medium.com/max/5500/1*3sF7FapSsIfTKkiX84G5jg.jpeg" alt="detail-article" />
                            <p>
                                Sidewalk Labs is one of the most mysterious initiatives inside Alphabet (and formerly, Google). 
                                The company says it “imagines, designs, tests, and builds urban innovations to help cities meet their 
                                biggest challenges.” In other words, Sidewalk Labs wants to create the smart city of the future. 
                                In 2017, the company reached an agreement to build its first “city” in Toronto: A new neighborhood called 
                                Quayside developed on the city’s waterfront.
                            </p>
                            <p>
                                “What that city will look like has been almost entirely unknown—until now. Sidewalk Labs has shared its 
                                initial renderings and project plans for Quayside, which were developed alongside the architecture studios
                                Snøhetta and Heatherwick Studio. The plans are being made public months ahead of schedule. 
                                As a Sidewalk Labs spokesperson admits, the plans were leaked far ahead of time 
                                (they were published by the Toronto Star last week).
                            </p>
                            <p>
                                As a result, what you see here should still be considered a concept. It hasn’t even been approved by 
                                Alphabet itself, and everything would need to be green-lit by the city of Toronto and various regulators. 
                                Still, it’s a glimpse at one of the most-anticipated smart city plans in recent memory. 
                                Here are the most surprising details from the now-public plans.
                            </p>
                            <Button variant="contained" color="primary" >Share</Button>
                        </Content>
                    </Grid>
                    
                </Grid>
                
            </Wrapper>
            <Bottom>
                <Footer />
            </Bottom>
            
           
        </Fragment>
    )
}

export default Detail