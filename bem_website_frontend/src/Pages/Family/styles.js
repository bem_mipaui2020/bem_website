import styled from "styled-components"

export const Flex = styled.div`
    display: flex;
    flex-direction: ${({direction}) => direction};
    justify-content: ${({justify}) => justify};
    align-items: ${({alignItems}) => alignItems};
    flex-wrap: ${({wrap}) => wrap};
`

export const Header = styled.div`
    text-align: center;
`

export const Wrapper = styled.div`
    // display: flex;
    // flex-direction: row;
    // justify-content: center;
    margin-top: 100px;

    .leaders{
        width: 250px;
        height: 250px;
        @media (max-width: 500px){
            width: 200px;
            height: 200px;
        }
    }
    .name{
        font-weight: bold;
        font-size: 28px;
        color: #BB4628;
        text-align: center;
        @media (max-width: 500px){
            font-size: 20px;
        }
    }
    .position{
        font-size: 24px;
        font-weight: bold;
        text-align: center;
        opacity: 0.5;
        @media (max-width: 500px){
            font-size: 16px;
        }
    }
`

export const Bawah = styled.div`
    margin-top: 100px; 

    .bawahan{
        width: 200px;
        height: 200px;
        margin-top: 40px;
        @media (max-width: 500px){
            width: 160px;
            height: 160px;
        }
    }
    .name{
        font-weight: bold;
        font-size: 24px;
        color: #BB4628;
        text-align: center;
        @media (max-width: 500px){
            font-size: 20px;
        }
    }
    .position{
        font-size: 20px;
        font-weight: bold;
        text-align: center;
        opacity: 0.5;
        @media (max-width: 500px){
            font-size: 16px;
        }
    }
`