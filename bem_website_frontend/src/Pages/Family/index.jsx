import React, {Fragment} from "react"
import {Grid, Avatar, Paper} from "@material-ui/core"
import {Header, Flex, Wrapper, Bawah} from "./styles"
import Navbar from "../../Components/Navbar"
import Footer from "../../Components/Footer"
import Ketua from "./Media/Mahesa_Oktareza.jpg"
import Wakil from "./Media/Aisha_Nurtabina.jpg"
import Zaki from "./Media/Zaki_Al_Aziz.jpg"
import Annisaa from "./Media/Annisaa_Dwi.jpg"
import Farah from "./Media/Farah_Yuliandika.jpg"
import Ginanjar from "./Media/Ginanjar_Ariyasuta.JPG"
import Arif from "./Media/Muhammad_Arif.jpg"
import Pramediya from "./Media/Pramediya_Tanjungwuri.jpg"
import Triananda from "./Media/Triananda_Wiradhika.jpg"
import Yoshua from "./Media/Yoshua_Yosia.jpg"

const Family = () => {
    return(
        <Fragment>
            <Navbar />
                <Header>
                    <h1>Meet Our Leaders</h1>
                </Header>
                <Wrapper>
                    <Flex direction="row" justify="space-around" alignItems="center">
                        <Flex direction="column" justify="center" style={{margin: 0}}>
                            <Avatar alt="Remy Sharp" src={Ketua} className="leaders" />
                            <div style={{margin: 0}}>
                                <p className="name">Mahesa Oktareza</p>
                                <p className="position">Ketua Umum</p>
                            </div>
                            
                        </Flex>
                        <Flex direction="column" alignItems="center">
                            <Avatar alt="Remy Sharp" src={Wakil} className="leaders" />
                            <div style={{margin: 0}}>
                                <p className="name">Aisha Nurtbanina</p>
                                <p className="position">Wakil Ketua Umum</p>
                            </div>
                       
                        </Flex>
                    </Flex>
                    {/* <Grid container direction="column" justify="center" alignItems="center">
                        <Avatar alt="Remy Sharp" src={Ketua} className="leaders" />
                        <p className="name">Mahesa Oktareza</p>
                        <p className="position">Ketua Umum</p>
                    </Grid>
                    <Grid container direction="column" justify="center" alignItems="center">
                        <Avatar alt="Remy Sharp" src={Wakil} className="leaders" />
                        <p className="name">Aisha Nurtbanina</p>
                        <p className="position">Wakil Ketua Umum</p>
                    </Grid> */}
                </Wrapper>
                <Bawah>
                    {/* <Flex direction="row" justify="space-around" wrap="wrap">
                        <div style={{margin: 0}}>
                            <Avatar alt="Remy Sharp" src={Zaki} className="bawahan" />
                            <p className="name">Zaki Al Aziz</p>
                            <p className="position">Kepala Satuan Pegendali Internal</p>
                        </div>
                        <div style={{margin: 0}}>
                            <Avatar alt="Remy Sharp" src={Pramediya} className="bawahan" />
                            <p className="name">Pramediya Tanjungwuri</p>
                            <p className="position">Bendahara Controller</p>
                        </div>
                        <div style={{margin: 0}}>
                            <Avatar alt="Remy Sharp" src={Farah} className="bawahan" />
                            <p className="name">Farah Yuliandika</p>
                            <p className="position">Bendahara Treasurer</p>
                        </div>
                    </Flex>
                    <Flex direction="row" justify="space-around" wrap="wrap">
                        <div style={{margin: 0}}>
                            <Avatar alt="Remy Sharp" src={Yoshua} className="bawahan" />
                            <p className="name">Yoshua Yosia</p>
                            <p className="position">Koordinator Bidang Internal</p>
                        </div>
                        <div style={{margin: 0}}>
                            <Avatar alt="Remy Sharp" src={Annisaa} className="bawahan" />
                            <p className="name">Annisaa Dwi Maiikhsantiani</p>
                            <p className="position">Koordinator Bidang Komunikasi dan Informasi</p>
                        </div>
                        <div style={{margin: 0}}>
                            <Avatar alt="Remy Sharp" src={Triananda} className="bawahan" />
                            <p className="name">I Nyoman Triananda Wiradhika Putra</p>
                            <p className="position">Koordinator Bidang Kemahasiswaan</p>
                        </div>
                    </Flex> */}
                    <Grid container spacing={1}>
                        <Grid container direction="column" justify="center" alignItems="center" item md={4}>
                            <Avatar alt="Remy Sharp" src={Zaki} className="bawahan" />
                            <div style={{margin: 0}}>
                                <p className="name">Zaki Al Aziz</p>
                                <p className="position">Kepala Satuan Pegendali Internal</p>
                            </div>
                        </Grid>
                        <Grid container direction="column" justify="center" alignItems="center" item md={4}>
                            <Avatar alt="Remy Sharp" src={Pramediya} className="bawahan" />
                            <div style={{margin: 0}}>
                                <p className="name">Pramediya Tanjungwuri</p>
                                <p className="position">Bendahara Controller</p>
                            </div>
                            
                        </Grid>
                        <Grid container direction="column" justify="center" alignItems="center" item md={4}>
                            <Avatar alt="Remy Sharp" src={Farah} className="bawahan" />
                            <div style={{margin: 0}}>   
                                <p className="name">Farah Yuliandika</p>
                                <p className="position">Bendahara Treasurer</p>
                            </div> 
                        </Grid>

                        <Grid container direction="column" justify="center" alignItems="center" item md={4}>
                            <Avatar alt="Remy Sharp" src={Yoshua} className="bawahan" />
                            <div style={{margin: 0}}> 
                                <p className="name">Yoshua Yosia</p>
                                <p className="position">Koordinator Bidang Internal</p>
                            </div>
                            
                        </Grid>
                        <Grid container direction="column" justify="center" alignItems="center" item md={4}>
                            <Avatar alt="Remy Sharp" src={Annisaa} className="bawahan" />
                            <div style={{margin: 0}}>
                                <p className="name">Annisaa Dwi Maiikhsantiani</p>
                                <p className="position">Koordinator Bidang Komunikasi dan Informasi</p>
                            </div>
                        </Grid>
                        <Grid container direction="column" justify="center" alignItems="center" item md={4}>
                            <Avatar alt="Remy Sharp" src={Triananda} className="bawahan" />
                            <div style={{margin: 0}}>
                                <p className="name">I Nyoman Triananda Wiradhika Putra</p>
                                <p className="position">Koordinator Bidang Kemahasiswaan</p>
                            </div>
                        </Grid>

                       
                        
                    </Grid>

                    <Grid container spacing={2}>
                        <Grid container direction="column" justify="center" alignItems="center" item md={6}>
                                <Avatar alt="Remy Sharp" src={Ginanjar} className="bawahan" />
                                <p className="name">Ginanjar Ariyasuta Eka Nugraha</p>
                                <p className="position">Koordinator Bidang Sosial dan Politik</p>
                            </Grid>
                            <Grid container direction="column" justify="center" alignItems="center" item md={6}>
                                <Avatar alt="Remy Sharp" src={Arif} className="bawahan" />
                                <p className="name">Muhammad Arif Rahman</p>
                                <p className="position">Koordinator Bidang Minat dan Bakat</p>
                            </Grid>
                    </Grid>
                    
                </Bawah>
                <Footer />
        </Fragment>
    )
}

export default Family