import React, { Fragment, useState, useEffect } from "react";
import Aos from "aos";
import "aos/dist/aos.css";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import { Button, Paper, Grid, Link } from "@material-ui/core";
import { Header, Flex, Schedule, Article, Video, Card } from "./styles";
import Navbar from "../../Components/Navbar";
import Footer from "../../Components/Footer";
import Cover from "./Media/Cover.JPG";

const Homepage = () => {
  const [date, setDate] = useState();
  const handleChange = () => {
    const tanggal = new Date();
    setDate(tanggal);
  };

  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, []);
  return (
    <Fragment>
      <Header>
        <Navbar />
        <img src={Cover} className="cover" alt="Cover" />
      </Header>

      <Schedule data-aos="fade-up">
        {/* <Flex direction="row">
                    <div>   
                    <Calendar
                            onChange={handleChange}
                            value={date}
                        />
                    </div>
                
                </Flex> */}
        <Grid container direction="column" justify="center" alignItems="center">
          <h1>Kalender Biru Merah</h1>
          <br />
          <Paper className="calendar">Calendar</Paper>
          <Button>See Calendar</Button>
        </Grid>
      </Schedule>

      <Article data-aos="fade-up">
        <Flex
          direction="row"
          justify="space-around"
          wrap="wrap"
          style={{ padding: 0, margin: 0 }}
        >
          <Flex direction="column" alignItems="center">
            <h2>Artikel 1</h2>
            <Link
              href="/detail"
              color="inherit"
              style={{ textDecoration: "none" }}
            >
              <Card>
                <img
                  src="https://img.timeinc.net/time/magazine/archive/covers/2013/1101130520_600.jpg"
                  alt="Avatar"
                  style={{ width: "100%" }}
                />
                <div className="container">
                  <h4>
                    <b>Newsletter BEM MIPA 2020</b>
                  </h4>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s...
                  </p>
                  <p className="time">17 Jul 2020</p>
                </div>
              </Card>
            </Link>
            <Button
              className="button"
              variant="contained"
              color="primary"
              href="/artikel"
            >
              See More
            </Button>
          </Flex>
          <Flex direction="column" alignItems="center">
            <h2>Artikel 1</h2>
            <Link
              href="/detail"
              color="inherit"
              style={{ textDecoration: "none" }}
            >
              <Card>
                <img
                  src="https://img.timeinc.net/time/magazine/archive/covers/2013/1101130520_600.jpg"
                  alt="Avatar"
                  style={{ width: "100%" }}
                />
                <div className="container">
                  <h4>
                    <b>Newsletter BEM MIPA 2020</b>
                  </h4>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s...
                  </p>
                  <p className="time">17 Jul 2020</p>
                </div>
              </Card>
            </Link>
            <Button
              className="button"
              variant="contained"
              color="primary"
              href="/artikel"
            >
              See More
            </Button>
          </Flex>
          <Flex direction="column" alignItems="center">
            <h2>Artikel 1</h2>
            <Link
              href="/detail"
              color="inherit"
              style={{ textDecoration: "none" }}
            >
              <Card>
                <img
                  src="https://img.timeinc.net/time/magazine/archive/covers/2013/1101130520_600.jpg"
                  alt="Avatar"
                  style={{ width: "100%" }}
                />
                <div className="container">
                  <h4>
                    <b>Newsletter BEM MIPA 2020</b>
                  </h4>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s...
                  </p>
                  <p className="time">17 Jul 2020</p>
                </div>
              </Card>
            </Link>
            <Button
              className="button"
              variant="contained"
              color="primary"
              href="/artikel"
            >
              See More
            </Button>
          </Flex>
        </Flex>
      </Article>

      <Video data-aos="fade-down">
        <Flex direction="column" justify="center" alignItems="center">
          <h1>Video Profil BEM MIPA UI</h1>
          <iframe
            src="https://www.youtube.com/embed/JztgJT547Vg"
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
          ></iframe>
        </Flex>
      </Video>

      <Footer></Footer>
    </Fragment>
  );
};

export default Homepage;
