import styled from 'styled-components'

export const Flex = styled.div`
    display: flex;
    flex-direction: ${({direction}) => direction};
    justify-content: ${({justify}) => justify};
    align-items: ${({alignItems}) => alignItems};
    flex-wrap: ${({wrap}) => wrap};
`

export const Space = styled.div`
    margin-top: 400px;
`

export const Header = styled.div`
    .cover{
        width: 100%;
        height: 44em;
    }
`

export const Schedule = styled.div`
    margin-top: 50px;
    h1{
        text-align: center;
    }
    button{
        background-color: #417288;
        color: white;
        padding: 15px;
        :hover{
            background: #417288;
        }
    }
    .calendar{
        width: 30em;
        height: 30em;
        text-align: center;
        padding: 20px;
        margin-bottom: 35px;
    }
    div{
        display: flex;
        flex-direction: column;
        
    }
`;

export const Article = styled.div`
    margin-top: 100px;

    h1{
        text-align: center;
    }
    h2{
        text-alignn: center;
        color: #BB4628;
    }
    .button{
        background-color: #417288;
        color: white;
        padding: 15px;
        margin-top: 30px;
        :hover{
            background: #417288;
        }
    }
    .article{
        width: 20em;
        height: 25em;
        border-radius: 10px;
        text-align: center;
        padding: 20px;
        margin-bottom: 35px;
    }
    .time{
        opacity: 0.5;
        margin-top: 3rem;
    }
    .hide{
        display: none;
    }
    .lala:hover + .hide{
        display: block
        // img{
        //     width: 100px;
        //     height: 100px;
        // }
    }
`

export const Card = styled.div`
    box-shadow: 0 4px 12px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 20em;
    border-radius: 10px;
    margin-bottom: 20px;
    cursor: pointer;

    :hover {
        box-shadow: 5px 8px 16px 6px rgba(0,0,0,0.2);
    }
    .container {
        padding: 2px 16px;
    }
    img{
        width: 100%;
        height: 20em;
        border-radius: 10px 10px 0 0;
    }
`


export const Video = styled.div`
    margin-top: 150px;
    h1{
        text-align: center;
    }
    button{
        background-color: #417288;
        color: white;
        padding: 15px;
        
        :hover{
            background: #417288;
        }
    }
    iframe{
        width: 920px;
        height: 515px;
        border-radius: 10px;
    }
    @media (max-width: 500px){
        iframe{
            width: 400px;
            height: 210px;

        }
    }
    

`
