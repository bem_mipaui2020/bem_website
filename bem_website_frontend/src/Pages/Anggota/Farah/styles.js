import styled from "styled-components"

export const Flex = styled.div`
    display: flex;
    flex-direction: ${({direction}) => direction};
    justify-content: ${({justify}) => justify};
    align-items: ${({alignItems}) => alignItems};
    flex-wrap: ${({wrap}) => wrap};
`


export const Wrapper = styled.div`
    width: 100%;
    height: 87.5vh;
    display: flex;
    margin-top: 20px;
    background-color: #EDE6DE;
    
`

export const Profile = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    float: left;
    margin-left: 100px;
    margin-top: 60px;
    h3{
        color: #BB4628;
        text-align: center;
    }
    img{
        width: 400px;
        height: 500px;
    }
`

export const Separator = styled.div`
    width: 25px;
    height: 100px;
    border-radius: 20px;
    background-color: #BB4628;
    
    
`

export const Box = styled.div`
    margin-top: 100px;
    .description{
        line-height: 0.3;
    }
    // div{
    //     display: flex;
    //     flex-direction: row;
    //     justify-content: center;
    //     align-items: center;
    // }
    
`