import React, { Fragment, useEffect } from "react";
import { Button, Paper, Grid } from "@material-ui/core";
import { Wrapper, Flex, Separator, Profile, Box } from "./styles";
import Navbar from "../../../Components/Navbar";

import Farah from "../Media/Farah_Yuliandika.png";

const AnggotaFarah = () => {

  
  return (
    <Fragment>
      <Navbar />
      <Wrapper>
        <Grid container>
          <Grid item md={6}>
            <Profile>
              <img src={Farah} alt="Zaki_photo" />
              <h3>Farah Yuliandika</h3>
            </Profile>
          </Grid>
          <Grid item md={5} style={{ textAlign: "center" }}>
            <Box>
                <Flex direction="column" justify="center" alignItems="center">
                    <h1>Bendahara Treasurer</h1>
                    <Separator />
                    <h4>Deskripsi Bidang</h4>
                </Flex>
                <Flex direction="column" justify="center" alignItems="center" >
                    <p>1. Memastikan setiap bidang dan program kerja berjalan tepat sasaran dan sesuai dengan tujuan, visi dan misi BEM FMIPA UI 2020</p>
                    <p>2. Mengadakan rangkaian penawaran terbuka untuk program kerja Penawaran Terbuka BEM FMIPA UI 2020</p>
                    <p>3. Melakukan pengawasan terhadap kinerja bidang dan program kerja sepanjang kepengurusan BEM FMIPA UI 2020</p>
                </Flex>
            </Box>
          </Grid>
        </Grid>
      </Wrapper>
    </Fragment>
  );
};

export default AnggotaFarah;
