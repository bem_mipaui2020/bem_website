import React, { Fragment } from "react";
import { Button, Paper, Grid } from "@material-ui/core";
import { Wrapper, Separator, Profile, Box } from "./styles";
import Navbar from "../../Components/Navbar";
import Zaki from "./Media/Zaki_Al_Aziz.png";

const Anggota = () => {
  return (
    <Fragment>
      <Navbar />
      <Wrapper>
        <Grid container>
          <Grid item md={6}>
            <Profile>
              <img src={Zaki} alt="Zaki_photo" />
              <h3>Zaki Al Aziz</h3>
            </Profile>
          </Grid>
          <Grid item md={5} style={{ textAlign: "center" }}>
            <Box>
              <h1>Kepala Satuan Pengendali Internal</h1>
              <div>
                <Separator />
              </div>
              <h4>Deskripsi Bidang</h4>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged.
              </p>
            </Box>
          </Grid>
        </Grid>
      </Wrapper>
    </Fragment>
  );
};

export default Anggota;
