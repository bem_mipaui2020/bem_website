import styled from "styled-components";

export const Flex = styled.div`
  display: flex;
  flex-direction: ${({ direction }) => direction};
  justify-content: ${({ justify }) => justify};
  align-items: ${({ alignItems }) => alignItems};
  flex-wrap: ${({ wrap }) => wrap};
`;

export const Header = styled.div`
  width: 100%;
  height: 900px;
  background: linear-gradient(
    180deg,
    rgba(187, 70, 40, 0.87) 0%,
    rgba(187, 70, 40, 0) 100%
  );
  img {
    width: 35em;
    height: 35em;
  }
  .departemen {
    font-size: 48px;
    font-weight: 700;
    width: 12em;
  }

  @media (max-width: 414px) {
    img {
      width: 23em;
      height: 23em;
      margin-left: 20px;
    }
    .departemen {
      font-size: 30px;
    }
  }
`;

export const Wrapper = styled.div`
  margin-top: 20px;
  .profile {
    display: block;
    width: 35em;
    height: 35em;
  }
  .name {
    font-size: 40px;
    font-weight: 500;
    color: #bb4628;
  }
  .description {
    width: 35em;
  }
  @media (max-width: 414px) {
    .profile {
      display: none;
    }
    .name {
      font-size: 27px;
    }
    .description {
      width: 15em;
    }
  }
`;
