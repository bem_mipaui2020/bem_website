import React, { Fragment } from "react";
import { Flex, Header, Wrapper } from "./styles";
import Navbar from "../../../../Components/Navbar";
import Footer from "../../../../Components/Footer";
import keilmuan from "./Media/keilmuan.png";
import Kepala from "./Media/kepala.png";
import deputi from "./Media/deputi1.png";
import deputi2 from "./Media/deputi2.png";

const Keilmuan = () => {
  return (
    <Fragment>
      <Navbar />
      <Header>
        <Flex direction="row" justify="center" wrap="wrap">
          <Flex direction="column" style={{ width: "40em", marginTop: "7em" }}>
            <img src={keilmuan} alt="group_photo" />
          </Flex>
          <Flex direction="column" justify="center" style={{ widht: "10em" }}>
            <h1 className="departemen">Departemen Keilmuan</h1>
          </Flex>
        </Flex>
      </Header>
      <Wrapper>
        <Flex direction="row" justify="center" wrap="wrap">
          <Flex
            direction="column"
            justify="center"
            style={{ widht: "30em", marginTop: "10em", marginLeft: "5em" }}
          >
            <div style={{ lineHeight: 0.6 }}>
              <h1 className="name">M. Attorik Falensky</h1>
              <h2 className="role">Kepala</h2>
            </div>
            <p className="description">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s. Lorem Ipsum is simply dummy text of the
              printing and typesetting industry. Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s.
            </p>
          </Flex>
          <Flex
            direction="column"
            style={{ width: "30em", marginRight: "8em" }}
          >
            <img src={Kepala} alt="group_photo" className="profile" />
          </Flex>
        </Flex>

        <Flex direction="row" justify="center" wrap="wrap">
          <Flex
            direction="column"
            style={{ width: "30em", marginRight: "8em" }}
          >
            <img src={deputi} alt="group_photo" className="profile" />
          </Flex>
          <Flex
            direction="column"
            justify="center"
            style={{ widht: "30em", marginTop: "10em" }}
          >
            <div style={{ lineHeight: 0.6 }}>
              <h1 className="name">Dianti Kurniatami</h1>
              <h2 className="role">Deputi</h2>
            </div>
            <p className="description">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s. Lorem Ipsum is simply dummy text of the
              printing and typesetting industry. Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s.
            </p>
          </Flex>
        </Flex>

        <Flex direction="row" justify="center" wrap="wrap">
          <Flex
            direction="column"
            justify="center"
            style={{ widht: "30em", marginTop: "10em", marginLeft: "5em" }}
          >
            <div style={{ lineHeight: 0.6 }}>
              <h1 className="name">Dinda Aulia Fuadi</h1>
              <h2 className="role">Deputi</h2>
            </div>
            <p className="description">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s. Lorem Ipsum is simply dummy text of the
              printing and typesetting industry. Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s.
            </p>
          </Flex>
          <Flex
            direction="column"
            style={{ width: "30em", marginRight: "8em" }}
          >
            <img src={deputi2} alt="group_photo" className="profile" />
          </Flex>
        </Flex>
      </Wrapper>

      <Footer />
    </Fragment>
  );
};

export default Keilmuan;
