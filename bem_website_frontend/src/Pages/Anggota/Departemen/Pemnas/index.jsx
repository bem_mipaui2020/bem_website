import React, { Fragment } from "react";
import { Flex, Header, Wrapper } from "./styles";
import Navbar from "../../../../Components/Navbar";
import Footer from "../../../../Components/Footer";
import pemnas from "./Media/pemnas.png";
import Kepala from "./Media/kepala.png";
import deputi from "./Media/deputi1.png";
import deputi2 from "./Media/deputi2.png";

const Pemnas = () => {
  return (
    <Fragment>
      <Navbar />
      <Header>
        <Flex direction="row" justify="center" wrap="wrap">
          <Flex direction="column" style={{ width: "40em", marginTop: "7em" }}>
            <img src={pemnas} alt="group_photo" />
          </Flex>
          <Flex direction="column" justify="center" style={{ widht: "10em" }}>
            <h1 className="departemen">Departemen Pembinaan dan Kaderisasi</h1>
          </Flex>
        </Flex>
      </Header>
      <Wrapper>
        <Flex direction="row" justify="center" wrap="wrap">
          <Flex
            direction="column"
            justify="center"
            style={{ widht: "30em", marginTop: "10em", marginLeft: "5em" }}
          >
            <div style={{ lineHeight: 0.6 }}>
              <h1 className="name">Alfira Farisya Rahmani</h1>
              <h2 className="role">Kepala</h2>
            </div>
            <p className="description">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s. Lorem Ipsum is simply dummy text of the
              printing and typesetting industry. Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s.
            </p>
          </Flex>
          <Flex
            direction="column"
            style={{ width: "30em", marginRight: "8em" }}
          >
            <img src={Kepala} alt="group_photo" className="profile" />
          </Flex>
        </Flex>

        <Flex direction="row" justify="center" wrap="wrap">
          <Flex
            direction="column"
            style={{ width: "30em", marginRight: "8em" }}
          >
            <img src={deputi} alt="group_photo" className="profile" />
          </Flex>
          <Flex
            direction="column"
            justify="center"
            style={{ widht: "30em", marginTop: "10em" }}
          >
            <div style={{ lineHeight: 0.6 }}>
              <h1 className="name">Emir Rakhim</h1>
              <h2 className="role">Deputi</h2>
            </div>
            <p className="description">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s. Lorem Ipsum is simply dummy text of the
              printing and typesetting industry. Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s.
            </p>
          </Flex>
        </Flex>

        <Flex direction="row" justify="center" wrap="wrap">
          <Flex
            direction="column"
            justify="center"
            style={{ widht: "30em", marginTop: "10em", marginLeft: "5em" }}
          >
            <div style={{ lineHeight: 0.6 }}>
              <h1 className="name">Lidya Lindasari</h1>
              <h2 className="role">Deputi</h2>
            </div>
            <p className="description">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s. Lorem Ipsum is simply dummy text of the
              printing and typesetting industry. Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s.
            </p>
          </Flex>
          <Flex
            direction="column"
            style={{ width: "30em", marginRight: "8em" }}
          >
            <img src={deputi2} alt="group_photo" className="profile" />
          </Flex>
        </Flex>
      </Wrapper>

      <Footer />
    </Fragment>
  );
};

export default Pemnas;
