import React, {Fragment} from "react"
import {Header, Article, Card} from "./styles"
import {Button, Paper, Grid} from "@material-ui/core"
import Navbar from "../../../Components/Navbar"
import Footer from "../../../Components/Footer"

const Article3 = () => {
    return(
        <Fragment>
            <Navbar />
            <Header>
                <h1>Article 3</h1>
            </Header>
            <Article>
                <div>
                    <Card>
                        <img src="https://img.timeinc.net/time/magazine/archive/covers/2013/1101130520_600.jpg" alt="Avatar" style={{width: "100%"}} />
                        <div className="container">
                            <h4><b>Newsletter BEM MIPA 2020</b></h4> 
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...
                            </p> 
                            <p style={{opacity: 0.5}}>17 Jul 2020</p>
                        </div>
                    </Card>
                </div>
                <div>
                    <Card>
                        <img src="https://img.timeinc.net/time/magazine/archive/covers/2013/1101130520_600.jpg" alt="Avatar" style={{width: "100%"}} />
                        <div className="container">
                            <h4><b>Newsletter BEM MIPA 2020</b></h4> 
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...
                            </p> 
                            <p style={{opacity: 0.5}}>17 Jul 2020</p>
                        </div>
                    </Card>
                </div>
                <div>
                    <Card>
                        <img src="https://img.timeinc.net/time/magazine/archive/covers/2013/1101130520_600.jpg" alt="Avatar" style={{width: "100%"}} />
                        <div className="container">
                            <h4><b>Newsletter BEM MIPA 2020</b></h4> 
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...
                            </p> 
                            <p style={{opacity: 0.5}}>17 Jul 2020</p>
                        </div>
                    </Card>
                </div>
                <div>
                    <Card>
                        <img src="https://img.timeinc.net/time/magazine/archive/covers/2013/1101130520_600.jpg" alt="Avatar" style={{width: "100%"}} />
                        <div className="container">
                            <h4><b>Newsletter BEM MIPA 2020</b></h4> 
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...
                            </p> 
                            <p style={{opacity: 0.5}}>17 Jul 2020</p>
                        </div>
                    </Card>
                </div>
                <div>
                    <Card>
                        <img src="https://img.timeinc.net/time/magazine/archive/covers/2013/1101130520_600.jpg" alt="Avatar" style={{width: "100%"}} />
                        <div className="container">
                            <h4><b>Newsletter BEM MIPA 2020</b></h4> 
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...
                            </p> 
                            <p style={{opacity: 0.5}}>17 Jul 2020</p>
                        </div>
                    </Card>
                </div>
                <div>
                    <Card>
                        <img src="https://img.timeinc.net/time/magazine/archive/covers/2013/1101130520_600.jpg" alt="Avatar" style={{width: "100%"}} />
                        <div className="container">
                            <h4><b>Newsletter BEM MIPA 2020</b></h4> 
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...
                            </p> 
                            <p style={{opacity: 0.5}}>17 Jul 2020</p>
                        </div>
                    </Card>
                </div>
                
            </Article>
            <Footer />
        </Fragment>
    )
}

export default Article3