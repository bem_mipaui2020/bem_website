import styled from 'styled-components'

export const Header = styled.div`
    width: 100%;
    height: 100px;
    margin-top: 30px;
    background-color: #417288;
    h1{
        text-align: center;
        color: white;
        padding: 25px;
    }
  
`

export const Article = styled.div`
    margin-top: 100px;
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    flex-wrap: wrap;
    align-items: center;

    h1{
        text-align: center;
    }
    h2{
        text-alignn: center;
        color: #BB4628;
    }
    button{
        background-color: #417288;
        color: white;
        padding: 15px;
        margin-top: 30px;
        :hover{
            background: #417288;
        }
    }
    .article{
        width: 20em;
        height: 25em;
        border-radius: 10px;
        text-align: center;
        padding: 20px;
        margin-bottom: 35px;
    }
    div{
        display: flex;
        flex-direction: column;
        margin-top: 15px;
        h3{
            text-align: center;
        }
        
    }
`

export const Card = styled.div`
    box-shadow: 0 4px 12px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 20em;
    border-radius: 10px;
    margin-bottom: 20px;
    margin-right: 30px;
    margin-left: 30px;
    cursor: pointer;

    :hover {
        box-shadow: 5px 8px 16px 6px rgba(0,0,0,0.2);
    }
    .container {
        padding: 2px 16px;
    }
    img{
        width: 100%;
        height: 20em;
        border-radius: 10px 10px 0 0;
    }
`