import React, { Fragment, useState, useEffect } from "react";
import axios from "axios";
import { Wrapper, Flex, Header, Post, Card } from "./styles";
import { Button, TextField, Link, CircularProgress } from "@material-ui/core";
import Navbar from "../../Components/Navbar";
import { headers, baseUrl } from "../../config";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";

const AdkesmaManagement = () => {
  const history = useHistory();
  const [value, setValue] = useState({
    caption: "",
    url: "",
  });
  const handleChange = (event) => {
    const values = event.target.value;
    setValue({
      ...value,
      [event.target.name]: values,
    });
    console.log(values);
  };
  const handlePost = (e) => {
    e.preventDefault();
    const adkesma = {
      caption: value.caption,
      url: value.url,
    };
    console.log(adkesma);
    axios.post(`${baseUrl}/create-adkesma`, adkesma).then((res) => {
      console.log(res);
      console.log(res.data);
    });
    window.location.reload();
  };

  const [items, setItems] = useState([]);
  useEffect(() => {
    axios.get(`${baseUrl}/adkesma`, headers()).then((res) => {
      const items = res.data.data;
      console.log(res);
      setItems(items);
    });
  }, []);
  console.log(items);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const [open, setOpen] = React.useState(false);
  const [name, setName] = useState("");
  const [id, setId] = useState("");
  const handleClickOpen = (caption, id) => {
    setOpen(true);
    setName(caption);
    setId(id);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDelete = () => {
    axios.delete(`${baseUrl}/delete-adkesma/${id}`).then((res) => {
      console.log(res);
    });
    window.location.reload();
  };

  return (
    <Fragment>
      <Navbar />
      <Wrapper>
        <Header>
          <h1>New Post</h1>
          <hr />
          <br />

          <div className="row">
            <div className="column" style={{ width: "30em" }}>
              <h3>Content</h3>
              <TextField
                id="outlined-basic"
                label="Content"
                variant="outlined"
                name="caption"
                value={value.caption}
                onChange={handleChange}
              />
            </div>
            <div
              className="column"
              style={{ marginLeft: "30px", width: "30em" }}
            >
              <h3>Url</h3>
              <TextField
                id="outlined-basic"
                label="Url"
                variant="outlined"
                name="url"
                value={value.url}
                onChange={handleChange}
              />
            </div>
          </div>
        </Header>
        <br />
        <Button
          variant="contained"
          color="primary"
          className="publishBtn"
          onClick={handlePost}
        >
          Post
        </Button>

        <Post>
          {items.length === 0 ? (
            <Flex direction="row" justify="center">
              <CircularProgress />
            </Flex>
          ) : (
            <Flex direction="row" justify="space-around" wrap="wrap">
              {items.map((items) => (
                <Link
                  onClick={() => handleClickOpen(items.caption, items.id)}
                  style={{ textDecoration: "none" }}
                >
                  <Card>
                    <p>{items.caption}</p>
                  </Card>
                </Link>
              ))}
            </Flex>
          )}

          <Dialog
            fullScreen={fullScreen}
            open={open}
            onClose={handleClose}
            aria-labelledby="responsive-dialog-title"
          >
            <DialogTitle id="responsive-dialog-title">{`Are you sure want to delete ${name}?`}</DialogTitle>
            <DialogContent>
              <DialogContentText>
                If you delete this content, it will not appear again in suara
                adkesma page
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button autoFocus onClick={handleClose} color="primary">
                Cancel
              </Button>
              <Button onClick={handleDelete} color="primary" autoFocus>
                Delete
              </Button>
            </DialogActions>
          </Dialog>
        </Post>
      </Wrapper>
    </Fragment>
  );
};

export default AdkesmaManagement;
