import styled from "styled-components"

export const Flex = styled.div`
    display: flex;
    flex-direction: ${({direction}) => direction};
    justify-content: ${({justify}) => justify};
    align-items: ${({alignItems}) => alignItems};
    flex-wrap: ${({wrap}) => wrap};
`

export const Wrapper = styled.div`
    width: 100vw;
    height: 100vh;
    // display: flex;
    margin-top: 20px;
    // padding: 100px;

    hr{
        width: 53em;
        border: 1px solid #BB4628;
        float: left;
    }
    
    .draftBtn{
        color: #BB4628;
        text-decoration: none;
    }
    .publishBtn{
        text-decoration: none;
        background: #BB4628;
        color: white;
        margin-left: 7em;
        width: 100px;
        :hover{
            background-color: #AA4615;
        }
    }
    
`
export const Header = styled.div`
    padding-left: 100px;
    padding-top: 30px;
    .row{
        // width: 130em
        display: flex;
        flex-direction: row;
        justify-content: flex-start;
    }

    .column{
        // width: 150em
        display: flex;
        flex-direction: column;
        justify-content: space-around;
    }
    .uploadBtn{
        background: #BB4628;
        width: 200px;
        height: 50px;
        color: white;
        :hover{
            background-color: #AA4615;
        }
    }
    
`

export const Post = styled.div`
    margin-top: 100px;

`

export const Card = styled.div`
    margin-top: 20px;
    width: 400px;
    height: 50px;
    padding: 15px;
    // padding-left: 10px;
    border: 1px solid black;
    border-left: 4px solid #417288;
    :hover{
        background: #417288;
        transition: 0.5s;
        color: white;
        cursor: pointer;
    }
    
`