import React, {Fragment, useState} from 'react'
import axios from 'axios'
import { EditorState, convertToRaw } from 'draft-js';
import draftToHtml from "draftjs-to-html";
import { Editor } from 'react-draft-wysiwyg';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { Button, Paper, Grid, TextField } from "@material-ui/core";
import { Wrapper, Header, Content } from "./styles";
import Navbar from "../../Components/Navbar";
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import {baseUrl} from "../../config"
import { useHistory } from 'react-router-dom';

const CreateArticle = () => {

    const history = useHistory()
    const [editorState, setEditorState] = useState(EditorState.createEmpty())
    const [image, setImage] = useState('')
    const [pic, setPic] = useState('')
    const [loading, setLoading] = useState(false)

    const uploadImage = async e => {
        const files = e.target.files
        const data = new FormData()
        data.append('file', files[0])
        data.append('upload_preset', 'bem_mipa')
        setLoading(true)
        const res = await fetch(
            'https://api.cloudinary.com/v1_1/dgon55lhq/image/upload',
            {
                method: 'POST',
                body: data
            }
        )
        const file = await res.json()
        setImage(file.secure_url)
        setLoading(false)
    }
    console.log(image)

    const onEditorStateChange = (editorState) => {
        setEditorState(editorState)
    }

    const [value, setValue] = useState({
        title: '',
        writer: '',
        content: '',
        thumbnail_photo: '',
    })

    const handleChange = (event) => {
        const values = event.target.value
        setValue({
            ...value,
            [event.target.name]: values
        })
        console.log(values)
    }

    const handlePost = () => {
        var regex = /(<([^>]+)>)/ig
        const text = draftToHtml(convertToRaw(editorState.getCurrentContent()));
        const result = text.replace(regex, "")
        const article = {
            title: value.title,
            writer: value.writer,
            content: result,
            thumbnail_photo: image
        }

        axios.post(`${baseUrl}/create-article`, article)
        .then(res => {
            // e.preventDefault()
            console.log(res)
            console.log(res.data)
            alert("Your article has been successfully posted")
            history.push('/artikel-management')
        })
    } 

    // const uploadImageCallBack = async (file) => {
    //     var formData = new FormData();
    //     formData.append("file", file);
    //     try {
    //         const { data } = await API.post(ContentManagementURL.UploadContent, formData);
    //         const image = { data: { link: data.original } };
    //         return image;
    //     } catch (e) {
    //       alert.error(e);
    //     }
    //   };
    
    // const uploadArticleImage = async e => {
    //     const files = e.target.files
    //     const data = new FormData()
    //     data.append('file', files[0])
    //     data.append('upload_preset', 'bem_mipa')
    //     setLoading(true)
    //     const res = await fetch(
    //         'https://api.cloudinary.com/v1_1/dgon55lhq/image/upload',
    //         {
    //             method: 'POST',
    //             body: data
    //         }
    //     )
    //     const file = await res.json()
    //     setPic(file.secure_url)
    //     // const picture = {res : {link: file.secure_url}}
    //     console.log(pic)
    //     return pic
    //     // setLoading(false)
        
    // }
    // console.log(pic)

    return(
        <Fragment>
            <Navbar />
            <Wrapper>
                <Header>
                    <h1>New Article</h1>
                    <hr />
                    <br />
                    <div className="row">
                        <div className="column">
                            <h3>Title</h3>
                            <TextField 
                                id="outlined-basic" 
                                label="Title" 
                                variant="outlined"
                                name="title"
                                values={value.title}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="column" style={{marginLeft: '30px'}}>
                            <h3>Writer</h3>
                            <TextField 
                                id="outlined-basic" 
                                label="Writer" 
                                variant="outlined" 
                                name="writer"
                                values={value.writer}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="column" style={{marginLeft: '230px'}}>
                            <h3>Thumbnail Photo</h3>
                            <input
                                type="file"
                                name="file"
                                placeholder="upload an image"
                                id="contained-button-file"
                                style={{display: 'none'}}
                                onChange={uploadImage}
                            />
                            {loading ? (
                                <h3>Loading...</h3>
                            ) : (
                                <img src={image} style={{width: '300px'}} />
                            )}
                            <label htmlFor="contained-button-file">
                                <Button
                                    variant="contained"
                                    color="default"
                                    className="uploadBtn"
                                    component="span"
                                    startIcon={<CloudUploadIcon />}
                                    onClick={() => setImage(image)}
                                >
                                    Upload
                                </Button>
                            </label>   
                        </div>
                    </div>
                    
                    
                </Header>
                <Content>
                <Editor
                    editorState={editorState}
                    wrapperClassName="wrapper"
                    editorClassName="editor"
                    name="content"
                    onEditorStateChange={onEditorStateChange}
                    toolbar={{
                    inline: { inDropdown: true },
                    list: { inDropdown: true },
                    //   textAlign: { inDropdown: true },
                    link: { inDropdown: true },
                    history: { inDropdown: true },
                    image: {
                        // uploadCallback: pic,
                        alt: { present: true, mandatory: true },
                        previewImage: true,
                        uploadEnabled: true,
                    },
                    embedded: {
                        // embedCallback: embedYoutubeCallBack,
                    },
                    remove: {
                        className: "remove-icon",
                    },
                    }}
                    hashtag={{
                    separator: "  ",
                    trigger: "#",
                    }}
                />
                
                </Content>
                <div className="row" style={{marginTop: '30px', float: 'right', paddingRight: '17em'}}>
                    <Button variant="outlined"  className="draftBtn">
                        Save as a draft
                    </Button>
                    <Button variant="contained" color="secondary" className="publishBtn" onClick={handlePost}>
                        Publish
                    </Button>
                </div>
                
               
            </Wrapper>
        </Fragment>
    )
}

export default CreateArticle

