import styled from "styled-components"

export const Wrapper = styled.div`
    width: 100vw;
    height: 100vh;
    // display: flex;
    margin-top: 20px;
    // padding: 100px;

    hr{
        width: 53em;
        border: 1px solid #BB4628;
        float: left;
    }
    
    .draftBtn{
        color: #BB4628;
        text-decoration: none;
    }
    .publishBtn{
        text-decoration: none;
        background: #BB4628;
        color: white;
        margin-left: 20px;
        :hover{
            background-color: #AA4615;
        }
    }
    
`

export const Header = styled.div`
    padding-left: 100px;
    padding-top: 30px;
    .row{
        // width: 130em
        display: flex;
        flex-direction: row;
        justify-content: flex-start;
    }

    .column{
        // width: 150em
        display: flex;
        flex-direction: column;
        justify-content: space-around;
    }
    .uploadBtn{
        background: #BB4628;
        width: 200px;
        height: 50px;
        color: white;
        :hover{
            background-color: #AA4615;
        }
    }
    
`

export const Content = styled.div`
    margin-left: 6em;
    margin-top: 2em;
    padding-left: 30px;
    padding-top: 30px;
    padding-right: 400px;
    width: 40em;
    border-style: ridge;
`
