import styled from 'styled-components'

export const Flex = styled.div`
    display: flex;
    flex-direction: ${({direction}) => direction};
    justify-content: ${({justify}) => justify};
    align-items: ${({alignItems}) => alignItems};
    flex-wrap: ${({wrap}) => wrap};
`

export const Header = styled.div`
    button{
        background-color: #BB4628;
        color: white;
        padding: 15px;
        margin-top: 30px;
        :hover{
            background: #BB4628;
        }
    }
  
`

export const Article = styled.div`
    margin-top: 50px;
    h1{
        text-align: center;
    }
    h2{
        text-alignn: center;
        color: #BB4628;
    }
    .button{
        background-color: #417288;
        color: white;
        padding: 15px;
        margin-top: 30px;
        :hover{
            background: #417288;
        }
    }
    .article{
        width: 20em;
        height: 25em;
        border-radius: 10px;
        text-align: center;
        padding: 20px;
        margin-bottom: 35px;
    }
    .time{
        opacity: 0.5;
    }

      
    }
`

export const Card = styled.div`
    box-shadow: 0 4px 12px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 20em;
    height: 37em;
    border-radius: 10px;
    margin-bottom: 20px;
    margin-right: 30px;
    margin-left: 30px;
    cursor: pointer;

    :hover {
        box-shadow: 5px 8px 16px 6px rgba(0,0,0,0.2);
    }
    .container {
        padding: 2px 16px;
    }
    .text{
        height: 80px;
    }
    img{
        width: 100%;
        height: 20em;
        border-radius: 10px 10px 0 0;
    }
`

export const Option = styled.div`
  position: absolute;
  z-index: 1;
  width: 110px;
  height: 128px;
  border-radius: 4px;
  border: solid 1px #0185d4;
  background-color: white;
  top: 55em;
//   left: 20em;
  bottom: 0;
//   padding-top: 10px;
    line-height: 0.5px;

  p {
    font-size: 14px;
    color: #3f3f3f;
    // padding-top: 2px;
    margin-left: 5px;
    margin-top: 10px;
  }

  div {
    display: flex;
    flex-direction: row;
    width: 108px;
    height: 36px;
    cursor: pointer;
    div:hover {
      background-color: #e8e8e8;
    }
  }

`;