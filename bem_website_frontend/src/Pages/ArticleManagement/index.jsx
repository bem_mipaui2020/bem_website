import React, { Fragment, useState, useEffect } from "react";
import axios from "axios";
import { Header, Article, Card, Option, Flex } from "./styles";
import { Button, Menu, MenuItem, Link } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Footer from "../../Components/Footer";
import Navbar from "../../Components/Navbar";
import { headers, baseUrl } from "../../config";

const ArticleManagement = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [id, setId] = useState("");
  const handleClick = (anchorEl, id) => {
    setId(id);
    console.log(id);
    setAnchorEl(anchorEl);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  // const [id, setId] = useState('')
  // const handleClickOpen = (id) => {
  //     setId(id)
  // };

  const [items, setItems] = useState([]);

  useEffect(() => {
    axios.get(`${baseUrl}/article`, headers()).then((res) => {
      const items = res.data.data;
      console.log(res);
      setItems(items);
    });
  }, []);

  const handleDelete = () => {
    axios.delete(`${baseUrl}/delete-article/${id}`).then((res) => {
      console.log(res);
    });
    alert("Article successfully deleted!");
    window.location.reload();
  };

  const getText = (text) => {
    const removeTag = text.replace(/<[^>]+>/g, "");
    if (removeTag.length > 100) return `${removeTag.substring(0, 150)}...`;
    else return removeTag;
  };

  return (
    <Fragment>
      <Navbar />
      <Header>
        <Flex direction="column" justify="center" alignItems="center">
          <h1>Latest Article</h1>
          <Button>
            <Link
              href="/create-article"
              style={{ textDecoration: "none", color: "white" }}
            >
              Add New Article
            </Link>
          </Button>
        </Flex>
      </Header>

      <Article>
        <Flex
          direction="row"
          justify="space-around"
          wrap="wrap"
          style={{ padding: 0, margin: 0 }}
        >
          {items.map((items) => (
            <Flex direction="column" alignItems="center">
              <Card>
                <img
                  src={items.thumbnail_photo}
                  alt="Avatar"
                  style={{ width: "100%" }}
                />
                <div className="container">
                  <h4>
                    <b>{items.title}</b>
                  </h4>
                  <p className="text">{getText(items.content)}</p>

                  <Flex direction="row" justify="space-between">
                    <Flex direction="column" style={{ lineHeight: 0.1 }}>
                      <p style={{ paddingTop: "30px" }}>
                        Writer : {items.writer}
                      </p>
                      <p className="time">{items.created_date}</p>
                    </Flex>
                    <MoreVertIcon
                      style={{ marginTop: "2em" }}
                      onClick={(event) =>
                        handleClick(event.currentTarget, items.id_article)
                      }
                    />

                    <Menu
                      id="simple-menu"
                      anchorEl={anchorEl}
                      keepMounted
                      open={Boolean(anchorEl)}
                      onClose={handleClose}
                    >
                      <MenuItem onClick={handleClose}>Detail</MenuItem>
                      <MenuItem onClick={handleDelete}>Delete</MenuItem>
                    </Menu>
                  </Flex>
                </div>
              </Card>
            </Flex>
          ))}
        </Flex>
      </Article>
      <Footer />
    </Fragment>
  );
};

export default ArticleManagement;
